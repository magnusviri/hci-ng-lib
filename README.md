# HciNgLib

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 9.1.4.

The project modernises the structure of the old hci-ng project to better conform to modern Angular Shared Library structure.

## Project organization and build scripts

There is a different library structure and the build process and dependencies of sub library projects are changed. dependencies in libraries have mostly been removed and moved to the parent project. All building is done from the parent and most of the dependencies end up in the node_modules of the parent project, rather than duplicated in each library. Necessary dependencies should be specified in the peerDependencies section of each library. Necessary @huntsman-cancer-institute libraries should be added to the dependencies section of the library (but only the minimum necessary version, rather than an exact version).

## Versioning

Starting with NG17, we will version the hci-ng libraries with the major version matching that of the NG version. So 17.x.x for NG 17.

## Building an individual library

```
ng build-<libraryName>
```

(e.g. build-authentication) See building-all-libraries for notes on inter-dependencies

## Building all libraries

```
npm run build-all
```

Note that where there are inter-dependencies between @huntsman-cancer-institute libraries the required dependencies must be published first. Since the publish task also builds, it's often handy to just run publish-all, instead. However, an effort has been made to set up the dependencies for each library with the minimum required version for each library (e.g. a dependency on ^17.0.0). This way if working on a 17.1.5 version, a library can be built even if the 17.1.5 version of another @huntsman-cancer-institute library has not yet been published (It can use any previous ^17.0.0 version). Note that publish-all will sort the libraries and build and publish them in the correct order to avoid these kinds of issues, anyway.

## Publishing an individual library

```
ng publish-<libraryName>
```

(e.g. publish-authentication)
 
## Publishing ALL libraries at once

```
npm run publish-libs
```

This is a gulp script that sorts the libraries based on inter-dependencies and publishes them in the correct order

## Push-versions

```
npm run push-versions
```

Pushes the version in package.json to each of the sub library projects. You use this when making a new version. Update the root project's package.json then use push-versions to update the version in each of the library projects at once

## Push-dependencies

```
npm run push-dependencies
```

Takes any of the dependency in the parent project's pushDependencies section and pushes them to each child library project. This is handy when upgrading NG versions, as you can set the required dependencies on the parent project and push them to each child library. This applies to both the versions of @huntsman-cancer-institute libraries as well as any other dependency in pushDependencies in the parent. This is a handy way of only needing to update dependencies in one place then apply them everywhere.

Note that if you need to add new peer dependencies to a library project, make sure they are added to the peerDependencies section in the parent project to keep this feature working properly

It is also convenient to list the minimum compatible version of @huntsman-cancer-institute libraries where there are inter-dependencies between various @huntsman-cancer-institute libraries. This way, when building a new version of one of the libraries it won't fail because another @huntsman-cancer-institute library has not yet been published with the updated version. The alternative is publishing each @huntsman-cancer-institute library in order, so that dependent @huntsman-cancer-institute libraries can be built


