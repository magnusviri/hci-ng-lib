
(function () {
  "use strict";

  const demoPath = "demo/node_modules/@huntsman-cancer-institute/reporting-framework";
  var cleanCSS = require("gulp-clean-css");
  const del = require("del");
  const fs = require("fs");
  var gulp = require("gulp");
  var path = require("path");
  var rename = require("gulp-rename");

  gulp.task("clean", function(done)  {
    done();
    return;
  });

  gulp.task("build", gulp.series("clean", function(done) {
    done();
    return gulp.src("src/lib/reporting-framework/**/*.css")
      .pipe(cleanCSS({}))
      .pipe(rename({
        extname: ".min.css"
      }))
      .pipe(gulp.dest("./dist/src"));
  }));

}());
