import {
    Component, ComponentFactory,
    ComponentFactoryResolver, ComponentRef,
    Input, OnDestroy,
    OnInit,
    ViewChild,
    ViewContainerRef
} from "@angular/core";
import {HttpClient, HttpParams} from "@angular/common/http";
import {Report} from "./model/report.model";
import {Param} from "./model/param.model";
import {ParameterComponent} from "./parameter/parameter.component";
import {Option} from "./model/option.model";
import {ReportTab} from "./model/report-tab.model";
import {Format} from "./model/format.model";
import {FormatComponent} from "./format/format.component";
import * as moment from "moment";
import {DatePipe} from "@angular/common";
import {ModalDismissReasons, NgbModal} from "@ng-bootstrap/ng-bootstrap";
import {DialogsService} from "./spinner/dialogs.service";
import { GridApi } from "ag-grid-community";

@Component({
    selector: "hci-reporting-framework",
    template: `
      <div style="height: 100vh">
        <resizable-section-divider class="flex-grow-1" [isVerticalLayout]="true">
          <div topSection class="d-flex flex-column" style="height: 100%; width: 100%; overflow: auto;">
            <!--can wrap just the grid with the tabs-->
            <mat-tab-group *ngIf="tabList.length > 0"
                           class="flex-grow-1">
              <mat-tab *ngFor="let tab of tabList"
                       label="{{tab.title}}"
                       class="flex-grow-1">
                <ng-template matTabContent>
                  <ag-grid-angular
                    class="ag-theme-balham full-width flex-grow-1"
                    style="min-height: 200px; height: 100%;"
                    (gridReady)="this.onTabbedGridReady($event, tab.name)"
                    [gridOptions]="this.gridOptions"
                    [rowSelection]="'single'"
                    (rowSelected)="setSelectedRow($event)"
                    [frameworkComponents]="frameworkComponents"
                    [rowData]="tabbedGridList[tab.name]">
                  </ag-grid-angular>
                </ng-template>
              </mat-tab>
            </mat-tab-group>

            <!--ngb-tabset *ngIf="tabList.length > 0"
                        destroyOnHide="false"
                        type="tabs"
                        class="flex-grow-1"
                        #reportTabSet="ngbTabset"
            >
              <ngb-tab *ngFor="let tab of tabList"
                       id="{{tab.name}}" title="{{tab.title}}"
                       style="border: 5px solid;" >
                <ng-template ngbTabContent >

                  <ag-grid-angular
                    class="ag-theme-balham full-width flex-grow-1"
                    style="min-height: 200px; height: 100%;"
                    (gridReady)="this.onTabbedGridReady($event, tab.name)"
                    [gridOptions]="this.gridOptions"
                    [rowSelection]="'single'"
                    (rowSelected)="setSelectedRow($event)"
                    [frameworkComponents]="frameworkComponents"
                    [rowData]="tabbedGridList[tab.name]">
                  </ag-grid-angular>
                </ng-template>
              </ngb-tab>

            </ngb-tabset-->

            <ag-grid-angular *ngIf="tabList.length <= 0"
                             class="ag-theme-balham full-width flex-grow-1"
                             style="min-height: 200px; height: 100%;"
                             (gridReady)="this.onMainGridReady($event)"
                             [gridOptions]="this.gridOptions"
                             [rowSelection]="'single'"
                             (rowSelected)="setSelectedRow($event)"
                             [frameworkComponents]="frameworkComponents"
                             [rowData]="fullGridList">
            </ag-grid-angular>
          </div>
          <div bottomSection class="d-flex flex-column" style="height: 80%; width: 100%; overflow: auto;">
            <div style="display: block;">
              <div class="row">
                <label style="margin-left: 25px; font-weight: bold">{{selectedReport.title}}</label>
                <button class="btn" *ngIf="selectedReport.help && selectedReport.help != ''"
                        (click)="openHelp(content)">
                  <i class="far fa-question-circle"></i>
                </button>
              </div>
            </div>
            <template #paramContainer></template>
            <div style="display: block;">
              <div class="row center-row">
                <template #formatContainer></template>
                <button *ngIf="selectedReport.name"
                        class="btn btn-success"
                        type="button"
                        (click)="onRunReport()"
                >Run Report</button>
              </div>
            </div>
          </div>
        </resizable-section-divider>
      </div>
      <ng-template #content let-modal>
        <div class="modal-header">
          <h4 class="modal-title" id="help-model-title">Report Help</h4>
          <button type="button" class="close" aria-label="Close" (click)="modal.dismiss('Cross click')">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body" >
          <div class="container" [innerHTML]="selectedReport.help">
          </div>
        </div>
        <div class="modal-footer">
          <button class="btn btn-primary" type="button" (click)="modal.dismiss('Ok click')">OK</button>
        </div>
      </ng-template>

    `,
    styles: [`
      :host ::ng-deep .mat-tab-body-wrapper{
        flex-grow: 1;
      }

      :host ::ng-deep .mat-tab-label{
        background-color: #98c7e0;
      }

      :host ::ng-deep .mat-tab-label-active{
        background-color: #def4ff;
      }

      .report-details{
        height: 100%;
        width: 100%;
        position: absolute
      }

      .center-row{
        margin-top: 10px;
        margin-left: 60vw;
      }
    `]
})
export class ReportingFrameworkComponent implements OnInit, OnDestroy {

    @Input() manageReportUrl: string; // /artadmin/legacy/api/hci.artadmin.controller.ManageReports
    @Input() commandPrefix: string; //  /artadmin/legacy/api/jsp
    @Input() dictAPILeadIn: string; // /artadmin/api/dictionaries
    @Input() reportsByType: string; //optional. usually "global" or "patient"

    @ViewChild("paramContainer", {read: ViewContainerRef, static: false}) paramContainer: ViewContainerRef;
    @ViewChild("formatContainer", {read: ViewContainerRef, static: false}) formatContainer: ViewContainerRef;

    fullGridList: any[] = [];
    tabbedGridList: any = {};


    gridOptions = {
      rowStyle: {
        "font-family": "Prompt, sans-serif"
      },
      rowHeight: 20,
      enableCellTextSelection: true,
      ensureDomOrder: true
    }; //enableCellTextSelection should be combined with ensureDomOrder
    frameworkComponents: any;

    mainGridApi: GridApi;
    tabbedGridApiList: any = {}; //a map of GridAPIs to tab names

    reportList: Report[] = [];
    tabList: ReportTab[] = [];
    tabTypeNameLookup: any = {}; //a kind of dict that maps the tab or type to the tab-name

    selectedReport: Report = new Report();
    selectedFormat: string;
    displayedParamComponentRefs: ComponentRef<any>[] = [];
    displayedFormatComponentRefs: ComponentRef<any>[] = [];

    constructor(private http: HttpClient,
                private resolver: ComponentFactoryResolver,
                private datePipe: DatePipe,
                private dialogsService: DialogsService,
                private modalService: NgbModal) {}

    ngOnInit(): void {

        if (this.manageReportUrl != null) {

            this.http.get(this.manageReportUrl,
                {
                    params: new HttpParams().set("action", "loadWithParams")
                }
            ).subscribe(
                (resp) => {
                    console.log(resp);
                    var templist = [];
                    var tempTabbedGridList = {};

                    var tablist = resp["reportTabs"];
                    for (let row of tablist) {
                        var tab: ReportTab = new ReportTab();
                        tab.name = row.name;
                        tab.title = row.title;
                        tab.type = row.type;
                        tab.tab = row.tab;
                        tab.datakey = row.datakey;

                        if (!this.reportsByType) {
                            this.tabList.push(tab);
                            if (tab.tab) {
                                this.tabTypeNameLookup[tab.tab] = tab.name;
                            }
                            if (tab.type) {
                                this.tabTypeNameLookup[tab.type] = tab.name;
                            }
                        }else {
                            if (this.reportsByType == tab.type) {
                                this.tabList.push(tab);
                                this.tabTypeNameLookup[tab.tab] = tab.name;
                            }
                        }

                        var tabGridList: any[] = [];
                        this.tabbedGridList[tab.name] = tabGridList;
                        tempTabbedGridList[tab.name] = [];
                    }


                    var list = resp["reportList"];
                    // console.log(list);
                    for (let row of list) {
                        var report: Report = new Report();
                        report.name = row.name;
                        report.title = row.title;
                        report.description = row.description;
                        report.className = row.className;
                        report.fileName = row.fileName;
                        report.type = row.type;
                        report.tab = row.tab;
                        report.datakey = row.datakey;
                        report.help = row.help;

                        const object = {
                            name: report.name,
                            title: report.title,
                            description: report.description
                        };


                        var tabname;
                        if (report.tab) {
                            tabname = this.tabTypeNameLookup[report.tab];
                        } else if (!this.reportsByType && report.type) {
                            tabname = this.tabTypeNameLookup[report.type];
                        }

                        if (tabname) {
                            var gridlist: any[] = tempTabbedGridList[tabname];

                            if(this.reportsByType) {
                                if (report.type === this.reportsByType ) {
                                    gridlist.push(object);
                                }
                            }else {
                                gridlist.push(object);
                            }
                        }

                        let params = row.params;
                        if (params) {

                            for (let p of params) {
                                // console.log(p);
                                var newParam: Param = new Param();
                                newParam.dictAPILeadIn = this.dictAPILeadIn;

                                newParam.caption = p.caption;
                                newParam.name = p.name;
                                newParam.type = p.type;
                                newParam.className = p.className;
                                newParam.valueXPath = p.valueXPath;
                                newParam.displayXPath = p.displayXPath;
                                newParam.displayOrder = p.displayOrder;
                                newParam.activeOnly = p.activeOnly;
                                newParam.sortField = p.sortField;
                                newParam.sortNumeric = p.sortNumeric;
                                newParam.filterField = p.filterField;

                                if (newParam.valueXPath) {
                                    newParam.valueXPath = newParam.valueXPath.replace("@", "");
                                }
                                if (newParam.displayXPath) {
                                    newParam.displayXPath = newParam.displayXPath.replace("@", "");
                                }
                                if (newParam.sortField) {
                                    newParam.sortField = newParam.sortField.replace("@", "");
                                }

                                let options = p.options;
                                if (options) {

                                    for (let o of options) {
                                        var newOption: Option = new Option();
                                        newOption.value = o.value;
                                        newOption.display = o.display;
                                        newOption.displayOrder = o.displayOrder;

                                        newParam.options.push(newOption);
                                    }
                                }

                                report.params.push(newParam);
                            }
                        }

                        let formats = row.formats;
                        if (formats) {

                            for (let f of formats) {
                                var newFormat: Format = new Format();
                                newFormat.name = f.name;
                                newFormat.caption = f.caption;
                                newFormat.displayOrder = f.displayOrder;

                                report.formats.push(newFormat);
                            }
                        }


                        this.reportList.push(report);

                        if (this.reportsByType) {
                            if(report.type === this.reportsByType) {
                                templist.push(object);
                            }
                        } else {
                            templist.push(object);
                        }
                    }

                    this.fullGridList = templist.slice();
                    console.log("gridlist: ");
                    console.log(this.fullGridList);

                    for(let tab of this.tabList){
                        this.tabbedGridList[tab.name] = tempTabbedGridList[tab.name].slice();
                    }

                }
            );
        }
    }

    public columnDef() : any[] {
      return [

        {
          field: "title",
          headerName: "Title",
          width: 300,
          sortable: true,
          resizable: true,
          editable: false},
        {
          field: "description",
          headerName: "Description",
          sortable: true,
          resizable: true,
          editable: false}
      ];

    }

    onMainGridReady(params : any) {

      this.mainGridApi = params.api;
      this.mainGridApi.setColumnDefs(this.columnDef());
      this.mainGridApi.setRowData(this.fullGridList);
      this.mainGridApi.sizeColumnsToFit();
    }

    onTabbedGridReady(params : any, tabname: string) {

      this.tabbedGridApiList[tabname] = params.api;
      this.tabbedGridApiList[tabname].setColumnDefs(this.columnDef());
      this.tabbedGridApiList[tabname].setRowData(this.tabbedGridList[tabname]);
      this.tabbedGridApiList[tabname].sizeColumnsToFit();
    }

    setSelectedRow(event) {

        //console.log(event);
        if(event.node.selected) {
          let reportName = event.node.data.name;
          console.log(event.node.data.name);

          var matchedReport: Report;
          for (let r of this.reportList) {
            if (r.name == reportName) {
              matchedReport = r;
            }
          }

          if (matchedReport != null) {
            this.selectedReport = matchedReport;
          } else {
            this.selectedReport = new Report();
          }

          this.createParamComponents();
        }
    }

    createParamComponents() {
        if(this.paramContainer) {
            this.paramContainer.clear();
        }
        if(this.formatContainer) {
            this.formatContainer.clear();
        }

        //destroy previous components and clear the displayedRef lists..?
        this.destroyComponentRefs();
        this.displayedParamComponentRefs = [];
        this.displayedFormatComponentRefs = [];

        for(let p of this.selectedReport.params){

            const factory: ComponentFactory<ParameterComponent> = this.resolver.resolveComponentFactory(ParameterComponent);
            var componentRef: ComponentRef<ParameterComponent> = this.paramContainer.createComponent(factory);
            this.displayedParamComponentRefs.push(componentRef);

            componentRef.instance.setParam(p);

            if(p.options.length > 0) {
                var entries: any[] = [];
                for(let o of p.options){
                    var e = {id: o.value, display: o.display};
                    entries.push(e);
                }
                componentRef.instance.entries = entries;
            }

        }

        var first: boolean = true;
        for(let f of this.selectedReport.formats){

            const factory2: ComponentFactory<FormatComponent> = this.resolver.resolveComponentFactory(FormatComponent);
            var componentRef2: ComponentRef<FormatComponent> = this.formatContainer.createComponent(factory2);
            this.displayedFormatComponentRefs.push(componentRef2);

            componentRef2.instance.format = f;
            componentRef2.instance.selectedValueChange.subscribe(
                (s) => {
                    this.selectedFormat = s;
                }
            );
            if(first) {//select the first format in the grouping
                componentRef2.instance.selectedValue = f.name;
                this.selectedFormat = f.name;
                first = false;
            }

        }

    }

    openHelp(content) {

        this.modalService.open(content, {ariaLabelledBy: "help-modal-title"})
            .result.then((result) => {
            // this.closeResult = `Closed with: ${result}`;
            console.log(result);

        }, (reason) => {
            var closeResult = `Dismissed ${this.getDismissReason(reason)}`;
            console.log(closeResult);

        });

    }

    private getDismissReason(reason: any): string {
        if (reason === ModalDismissReasons.ESC) {
            return "by pressing ESC";
        } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
            return "by clicking on a backdrop";
        } else {
            return  `with: ${reason}`;
        }
    }

    onRunReport() {

        if(this.selectedReport) {

            this.dialogsService.startDefaultSpinnerDialog();

            console.log("format:" + this.selectedFormat);
            if(!this.selectedFormat) {
                this.selectedFormat = "html";
            }

            let runParams = new HttpParams();
            runParams = runParams.append("format", this.selectedFormat);
            runParams = runParams.append("action", "show");
            runParams = runParams.append("reportName", this.selectedReport.name);

            runParams = this.getRunParameters(runParams);

            console.log(runParams);

            this.http.get(this.commandPrefix + this.selectedReport.className,
                {
                    params: runParams,
                    observe: "response",
                    responseType: "blob"
                }
            ).subscribe(
                (res) => {
                    console.log(res);

                    var filename;
                    if(this.selectedFormat != "html") {
                        filename = res.headers.get("content-disposition").split(";")[1].split("=")[1].replace(/\"/g, "");
                    }
                    var blob = new Blob([res.body], {type: res.body.type} ); //, {type: "application/vnd.ms-excel"}
                    var fileURL = window.URL.createObjectURL(blob);
                    console.log(fileURL);

                    const a: HTMLAnchorElement = document.createElement("a") as HTMLAnchorElement;
                    a.href = fileURL;
                    if(this.selectedFormat == "html") {
                        a.target = "_blank";
                    }else {
                        a.download = filename;
                    }
                    window.document.body.appendChild(a);
                    a.click();
                    window.document.body.removeChild(a);
                    URL.revokeObjectURL(fileURL);

                    this.dialogsService.stopAllSpinnerDialogs();
                }
            );
        }
    }

    getRunParameters(runParams: HttpParams): HttpParams {
        var newParams = runParams;

        for(let o of this.displayedParamComponentRefs){
            var param = o.instance.param;

            if(param.type == "multiselect") {
                var resList = "";
                for(let v of param.selectedValueList){
                    if(resList != "") {
                        resList += ",";
                    }
                    resList += v;
                }
                newParams = newParams.append(param.name, resList);
            }else {
                var value;
                if(param.selectedValue) { //so we don't get "undefined" sent back
                    if(param.type == "date") {
                        value = this.getDateString(param.selectedValue);
                    }else {
                        value = param.selectedValue;
                    }
                }else {
                    value = "";
                }
                newParams = newParams.append(param.name, value);
            }
        }
        return newParams;
    }

    getDateString(date: any): string {

        if (date instanceof Date) {
            if (!isNaN(date.getTime())) {
                return this.datePipe.transform(date, "MM/dd/yyyy");
            } else {
                return "";
            }
        }else if(moment.isMoment(date) ) {
            if(date != null) {
                return date.format("M/D/YYYY");
            }else {
                return "";
            }
        }else if(typeof date == "string") {
            return date;
        }else {
            return "";
        }
    }

    destroyComponentRefs() {
      for(let o of this.displayedParamComponentRefs){
        o.destroy();
      }
      for(let o of this.displayedFormatComponentRefs){
        o.destroy();
      }
    }

    ngOnDestroy(): void {
        this.destroyComponentRefs();
    }

    /*
    styles: [`
      .report-details{
        height: 100%;
        width: 100%;
        position: absolute
      }

      .center-row{
        margin-top: 10px;
        margin-left: 60vw;
      }

      .mat-tab-body-wrapper{
    flex-grow: 1;
  }

.tab-pane{
  height: 100%;
}
`]*/
}
