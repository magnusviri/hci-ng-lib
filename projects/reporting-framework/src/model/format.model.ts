
export class Format {

    public name: string;
    public caption: string;
    public displayOrder: number;

    public selected: string;
}
