
export class ReportTab {

    public name: string;
    public title: string;
    public type: string;
    public tab: string;
    public datakey: string;

}
