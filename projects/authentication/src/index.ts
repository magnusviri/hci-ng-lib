export {AuthenticationModule} from "./authentication.module";

/**
 * The injection tokens for service configuration.
 */
export {
  AUTHENTICATION_LOGOUT_PATH,
  AUTHENTICATION_SERVER_URL,
  AUTHENTICATION_TOKEN_ENDPOINT,
  AUTHENTICATION_DIRECT_ENDPOINT,
  AUTHENTICATION_ROUTE,
  AUTHENTICATION_MAX_INACTIVITY_MINUTES,
  AUTHENTICATION_USER_COUNTDOWN_SECONDS,
  AUTHENTICATION_IDP_INACTIVITY_MINUTES
} from "./authentication.service";

export {AUTHENTICATION_TOKEN_KEY} from "./authentication.provider";

export {AuthenticationComponent} from "./authentication.component"
export {DirectLoginComponent} from "./directlogin.component"
export {RouteGuardService} from "./route-guard.service"
export {AuthenticationService} from "./authentication.service"
export {TimeoutNotificationComponent} from "./timeout-notification.component";
export {AuthorizationInterceptor} from "./authorization.interceptor";