/*
 * Copyright (c) 2016 Huntsman Cancer Institute at the University of Utah, Confidential and Proprietary
 */
import {TestBed} from "@angular/core/testing";
import {Router, ActivatedRouteSnapshot, RouterStateSnapshot, NavigationExtras} from "@angular/router";
import {HttpClientTestingModule} from "@angular/common/http/testing";

import {} from "jasmine";
import {Observable, of} from "rxjs";

import {RouteGuardService} from "./route-guard.service";
import {AuthenticationService} from "./authentication.service";

const secureRoute: string = "/secure_route";

class MockActivatedRouteSnapshot {
  //no-op
}

class MockRouterStateSnapshot {
  public url: string = secureRoute;
}

class MockRouter {
  navigate(commands: any[], extras?: NavigationExtras): Promise<boolean> {
    return Promise.resolve(true);
  }
}

class MockAuthenticationService {
  public myRedirectUrl: string;

  set redirectUrl(redirectUrl: string) {
    this.myRedirectUrl = redirectUrl;
  }

  isAuthenticated(): Observable<boolean> {
    return of(true);
  }
}

describe("RouteGuardService Tests", () => {
  let mockAuthenticationService: MockAuthenticationService = new MockAuthenticationService();
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule
      ],
      providers: [
        {provide: RouterStateSnapshot, useValue: new MockRouterStateSnapshot()},
        {provide: ActivatedRouteSnapshot, useValue: new MockActivatedRouteSnapshot()},
        {provide: Router, useValue: new MockRouter()},
        {provide: AuthenticationService, useValue: mockAuthenticationService}
      ]
    });
  });

  it("Should set the redirectUrl on the AuthenticationService and navigate to the login route when user is unauthenticated.",
    () => {
      let authenticationRoute: string = "/login";
      let authenticationService: AuthenticationService = TestBed.get(AuthenticationService);
      let router: Router = TestBed.get(Router);
      let activatedRouteSnapshot: ActivatedRouteSnapshot = TestBed.get(ActivatedRouteSnapshot);
      let routerStateSnapshot: RouterStateSnapshot = TestBed.get(RouterStateSnapshot);
      let routeGuardService: RouteGuardService = new RouteGuardService(authenticationService, router, authenticationRoute);

      let navigateSpy = spyOn(router, "navigate");
      spyOn(authenticationService, "isAuthenticated").and.returnValue(of(false));


      routeGuardService.canActivate(activatedRouteSnapshot, routerStateSnapshot).subscribe((activate: boolean) => {
        expect(activate).toBeFalsy();
      });
      expect(mockAuthenticationService.myRedirectUrl).toBe(secureRoute);
      expect(navigateSpy).toHaveBeenCalledWith([authenticationRoute]);
    });

  it("Should return true when the user is authenticated.",
    () => {
      let authenticationService: AuthenticationService = TestBed.get(AuthenticationService);
      let router: Router = TestBed.get(Router);
      let activatedRouteSnapshot: ActivatedRouteSnapshot = TestBed.get(ActivatedRouteSnapshot);
      let routerStateSnapshot: RouterStateSnapshot = TestBed.get(RouterStateSnapshot);
      let routeGuardService: RouteGuardService = new RouteGuardService(authenticationService, router, "/login");

      let navigateSpy = spyOn(router, "navigate");

      routeGuardService.canActivate(activatedRouteSnapshot, routerStateSnapshot).subscribe((activate: boolean) => {
        expect(activate).toBeTruthy();
      });
      expect(navigateSpy).toHaveBeenCalledTimes(0);
    });
});
