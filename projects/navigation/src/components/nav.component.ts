import {
  ChangeDetectorRef,
  ComponentFactoryResolver,
  ElementRef,
  Input,
  isDevMode,
  Renderer2,
  SimpleChange,
  Type,
  ViewContainerRef,
  ViewRef,
  OnInit,
  AfterViewInit,
  OnChanges,
  OnDestroy,
  Component
} from "@angular/core";

import {Subscription} from "rxjs";
import {RoleEntity} from "@huntsman-cancer-institute/user";
import {ComponentInjector} from "@huntsman-cancer-institute/utils";
import {NavigationService} from "../services/navigation.service";

/**
 * This is the base class that all navigation components extend from.  This includes other standalone modules such
 * as a header or sidebar.  It contains common properties, configuration tools, and functions to add/insert other
 * NavComponents.
 * The config property is any object that contains a list of properties that specifies configuration options.  Common
 * properties include id, title, rootClass and roleName.  Extending classes may have their own properties such as extra
 * css classes.
 */
@Component(
  {
    template:
      `
      `,
    selector: "hci-nav"
  }
)
export class NavComponent implements OnInit, AfterViewInit, OnChanges, OnDestroy {

  @Input() config: any = {
    rendered: true
  };
  @Input("navId") id: string;
  @Input() isRoot: boolean = false;
  @Input() type: Type<any> | string;
  @Input() title: string;
  @Input() rootClass: string;
  @Input() roleName: string;
  @Input() authenticatedKey: string = "AUTHC";
  @Input() notAuthenticatedKey: string = "NOT_AUTHC";
  @Input("children") inputChildren: any[] = [];
  @Input() isCCR: boolean = false;
  /**
   * An array of all component instances of children.  It is important to note that we try to render everything in the
   * children array.  However, if roles are not met, some children may not be rendered so no component instance will
   * exist for them.
   */
  children: NavComponent[] = [];

  typeName: string;

  elementRef: ElementRef;
  renderer: Renderer2;
  resolver: ComponentFactoryResolver;
  navigationService: NavigationService;
  changeDetectorRef: ChangeDetectorRef;

  afterViewInit: boolean = false;
  initialized: boolean = false;

  /**
   * A set of container names.  Some components may not define a container while others just have
   * one (a "default" container).
   */
  containerSet: Set<string> = new Set<string>();

  /**
   * A map of IDs to the corresponding viewRef.  When we dynamically inject a new component, we first pull the created
   * ComponentRef and from there its ViewRef.  We need the ViewRef to determine the component's position in the
   * ViewContainerRef.  With a set of ViewRefs, we can detach them from the container and re-add them in a different order
   * based on the component's sortOrder (which is based on the position in the children array.  This map provides the
   * component to view relationship.
   */
  private viewRefMap: Map<string, ViewRef> = new Map<string, ViewRef>();

  /**
   * A reference to the parent component.  When a child is added, a reference to the parent is automatically set.
   */
  private parent: NavComponent;

  subscriptions: Subscription = new Subscription();

  constructor(elementRef: ElementRef, renderer: Renderer2, resolver: ComponentFactoryResolver, navigationService: NavigationService, changeDetectorRef: ChangeDetectorRef) {
    this.elementRef = elementRef;
    this.renderer = renderer;
    this.resolver = resolver;
    this.navigationService = navigationService;
    this.changeDetectorRef = changeDetectorRef;

    this.containerSet.add("default");
  }

  ngOnInit(): void {
    if (this.isRoot) {
      this.subscriptions.add(this.navigationService.getUserRoles().subscribe((userRoles: RoleEntity[]) => {
        if (isDevMode()) {
          console.debug("User Roles changed.  Refresh children.");
        }
        this.refreshChildren();
      }));
    }
  }

  ngAfterViewInit(): void {
    if (isDevMode()) {
      console.debug("NavComponent.ngAfterViewInit");
    }

    this.afterViewInit = true;
    this.checkInitialized();
  }

  /**
   * If config is bound by input, reset the config when it changes.  An important note, if anything in the config was
   * updated by updateConfig only, then those options will be lost.
   *
   * @param {{[p: string]: SimpleChange}} changes
   */
  ngOnChanges(changes: { [propName: string]: SimpleChange }): void {
    if (isDevMode()) {
      console.debug("ngOnChanges:");
      console.debug(changes);
    }

    for (let change of Object.keys(changes)) {
      if (change !== "config") {
        this[change] = changes[change].currentValue;

        if (change === "inputChildren") {
          this.config["children"] = changes[change].currentValue;
        } else {
          this.config[change] = changes[change].currentValue;
        }
      }
    }

    if (changes["config"]) {
      this.setConfig(this.config);
    }

    if (changes["id"]) {
      this.setId();
    }
    if (changes["rootClass"]) {
      this.setRootClass();
    }
    if (changes["inputChildren"]) {
      this.setChildren();
    }
  }

  ngOnDestroy(): void {
    if (this.changeDetectorRef) {
      this.changeDetectorRef.detach();
    }

    this.subscriptions.unsubscribe();
  }

  checkInitialized(): void {
    if (this.afterViewInit) {
      this.initialized = true;
      this.updateConfig(this.config);
      this.detectChanges();
      this.postInit();
    }
  }

  postInit(): void {
  }

  getNavigationService() {
    return this.navigationService;
  }

  /**
   * Remove all children.
   */
  clear(): void {
    if (!this.initialized) {
      return;
    }

    if (isDevMode()) {
      console.debug("NavComponent.clear");
    }

    for (let i = 0; i < this.children.length; i++) {
      this.remove(this.children[i], i);
    }

    this.children = [];
  }

  refresh() {
    let isRendered: boolean = false;
    if (this.config.rendered !== undefined) {
      isRendered = this.config.rendered;
    }
  }

  /**
   * Checks children and sees if there is a change in rendering status.  If a permission goes away, a component that
   * is currently rendered might need to be removed.  Recursively move through the tree of children.
   */
  refreshChildren() {
    if (!this.initialized) {
      return;
    }

    if (this.config.children) {
      for (let i = 0; i < this.config.children.length; i++) {
        if (isDevMode()) {
          console.debug("refreshChildren: " + i);
        }

        let render = this.canRender(this.config.children[i]);

        let isRendered: boolean = false;
        if (this.config.children[i].rendered !== undefined) {
          isRendered = this.config.children[i].rendered;
        }

        if (render && !isRendered) {
          this.add(this.config.children[i], i);
        } else if (!render && isRendered) {
          this.remove(this.config.children[i], i);
        }
      }
    }

    // Recursively check children
    for (let component of this.children) {
      component.refreshChildren();
    }

    this.sortContainers();

    this.detectChanges();
  }

  /**
   * Using the sortOrder property of each component, detach each ViewRef from the container, and re-attach based
   * on the sortOrder.
   */
  sortContainers() {
    if (this.config.children === undefined || !this.initialized) {
      return;
    }

    // Sort the children.  It doesn't matter that this list may contain children in multiple containers.
    let sortedChildren = this.config.children.sort((a: any, b: any) => {
      if (a.sortOrder < b.sortOrder) {
        return -1;
      } else if (a.sortOrder > b.sortOrder) {
        return 1;
      } else {
        return 0;
      }
    });

    for (let containerName of this.containerSet) {
      let container: ViewContainerRef = this.getContainer(containerName);

      if (!container) {
        return;
      }

      for (let i = 0; i < sortedChildren.length; i++) {
        if (!sortedChildren[i].id) {
          continue;
        } else if (sortedChildren[i].container !== containerName) {
          // Children is a list possibly spanning multiple containers.  Skip if not on the current container.
          continue;
        }

        if (this.viewRefMap.get(sortedChildren[i].id)) {
          if (isDevMode()) {
            console.debug("sortChildren: i: " + i + ", id: " + sortedChildren[i].id + ", destroyed: " + this.viewRefMap.get(sortedChildren[i].id).destroyed);
          }

          if (!this.viewRefMap.get(sortedChildren[i].id).destroyed) {
            container.insert(this.viewRefMap.get(sortedChildren[i].id));
          } else {
            this.viewRefMap.delete(sortedChildren[i].id);
          }
        }
      }
    }
  }

  /**
   * Remove a component by id from its parent container.
   *
   * @param componentConfig
   * @param {number} index
   */
  remove(componentConfig: any, index: number) {
    if (!this.config.children || !this.config.children[index]) {
      console.warn("NavComponent: Child " + index + " does not exist on " + this.config.id);
      return;
    }

    if (isDevMode()) {
      console.debug("NavComponent.remove: " + componentConfig.id);
    }
    if (componentConfig.children) {
      for (let i = 0; i < componentConfig.children.length; i++) {
        let renderedChild: NavComponent = this.getChild(componentConfig.children[i].id);
        if (renderedChild) {
          renderedChild.remove(componentConfig.children[i], i);
        }
      }
    }

    let container: ViewContainerRef;

    if (componentConfig.container) {
      container = this.getContainer(componentConfig.container);
    } else {
      container = this.getContainer();
    }
    if (this.viewRefMap.get(componentConfig.id)) {
      container.remove(container.indexOf(this.viewRefMap.get(componentConfig.id)));
      this.viewRefMap.delete(componentConfig.id);
    }
    componentConfig.rendered = false;
    this.config.children[index].rendered = false;

    this.removeChild(componentConfig.id);
  }

  removeById(id: any): void {
    if (isDevMode()) {
      console.debug("NavComponent.removeById: " + id);
    }

    for (let i = 0; i < this.children.length; i++) {
      if (this.children[i].id == id) {
        this.remove(this.children[i], i);

        return;
      }
    }

    throw new Error("removeById: ID " + id + " not found.");
  }

  /**
   * Checks to see if the component can be rendered based on user roles.  A role can be any string, but there are hardcoded
   * roles NOT_AUTHC and AUTHC.  If no roleName is specified, the component is always rendered.  If the roleName is
   * NOT_AUTHC, the component will only be rendered if there is no authenticated user.  If the roleName is AUTHC, the
   * component will be rendered if the user is authenticated.  If the roleName is any string and matches a user's role,
   * then the component will be rendered.
   *
   * @param componentConfig
   * @returns {boolean}
   */
  canRender(componentConfig: any): boolean {
    let render: boolean = true;

    if (componentConfig.roleName) {
      render = false;
      let userRoles: RoleEntity[] = this.navigationService.getUserRoles().getValue();

      if (componentConfig.roleName === this.notAuthenticatedKey && !userRoles) {
        render = true;
      } else if (componentConfig.roleName === this.authenticatedKey && userRoles) {
        render = true;
      } else if (userRoles && userRoles.length > 0) {
        render = userRoles.some((role: RoleEntity) => {
          return role.RoleName === componentConfig.roleName;
        });
      }
    }

    return render;
  }

  getChild(id: string): NavComponent {
    for (let i = 0; i < this.children.length; i++) {
      if (this.children[i].id === id) {
        return this.children[i];
      }
    }
    return undefined;
  }

  removeChild(id: string) {
    for (let i = 0; i < this.children.length; i++) {
      if (this.children[i].id === id) {
        this.children.splice(i, 1);
      }
    }
  }

  getId(): string {
    return this.id;
  }

  /**
   * If no index is specified, add the component to the children.  In all cases, see if the component can be rendered.
   * If so, call addComponent to handle the injection.
   *
   * @param componentConfig
   * @param index
   */
  add(componentConfig: any, index?: number) {
    if (isDevMode()) {
      console.debug("NavComponent.add: " + index);
    }

    if (componentConfig.type === undefined) {
      throw new Error("The component must specify a 'type' (e.g. UlNavComponent).");
    }
    if (index === undefined) {
      if (this.config.children) {
        index = this.config.children.length;
      } else {
        index = 0;
        this.config.children = [];
      }

      this.config.children.push(componentConfig);
    }

    if (this.canRender(componentConfig)) {
      this.addComponent(componentConfig, index);
    }
  }

  /**
   * Injects the component described by the configuration object in to the view and creates an instance of the component.
   * If no container is specified, inject in to the "default" container.
   *
   * @param componentConfig
   * @param index
   */
  addComponent(componentConfig: any, index: number) {
    if (componentConfig.type === undefined) {
      throw new Error("The component must specify a 'type' (e.g. UlNavComponent).");
    }

    if (componentConfig.container === undefined) {
      componentConfig.container = "default";
    }
    let container: ViewContainerRef = this.getContainer(componentConfig.container);

    if (isDevMode()) {
      console.debug("NavComponent.addComponent: to: " + componentConfig.container);
    }

    let componentRef = ComponentInjector.getComponentRef(this.resolver, container, componentConfig.type);
    let componentInstance = <NavComponent>componentRef.instance;

    this.config.children[index].rendered = true;
    componentConfig.rendered = true;

    let id: string = componentConfig.id;
    if (!id) {
      id = this.navigationService.getIdNavComponent();
      componentConfig.id = id;
    }

    componentInstance.setParent(this);
    (<NavComponent>componentInstance).setConfig(componentConfig);
    this.children.push(componentInstance);
    this.viewRefMap.set(componentConfig.id, componentRef.hostView);
    //this.navigationService.registerComponent(componentConfig.id, componentInstance);
  }

  insertAfter(id: string, componentConfig: any) {
    if (componentConfig.type === undefined) {
      throw new Error("The component must specify a 'type' (e.g. UlNavComponent).");
    }

    let index: number = undefined;
    for (let i = 0; i < this.config.children.length; i++) {
      if (this.config.children[i].id === id) {
        index = i + 1;
        break;
      }
    }

    if (index <= this.config.children.length - 1) {
      componentConfig.sortOrder = index;
      for (let i = index + 1; i < this.config.children.length; i++) {
        this.config.children[i].sortOrder = this.config.children[i].sortOrder + 1;
      }
      this.config.children.splice(index, 0, componentConfig);
    } else {
      index = this.config.children.length;
      componentConfig.sortOrder = index;
      this.config.children.push(componentConfig);
    }

    if (this.canRender(componentConfig)) {
      this.addComponent(componentConfig, index);
    }

    this.sortContainers();
  }

  insertBefore(id: string, componentConfig: any) {
    if (componentConfig.type === undefined) {
      throw new Error("The component must specify a 'type' (e.g. UlNavComponent).");
    }

    let index: number = undefined;
    for (let i = 0; i < this.config.children.length; i++) {
      if (this.config.children[i].id === id) {
        index = i;
        break;
      }
    }

    if (index <= this.config.children.length - 1) {
      for (let i = index; i < this.config.children.length; i++) {
        this.config.children[i].sortOrder = this.config.children[i].sortOrder + 1;
      }
      componentConfig.sortOrder = index;
      this.config.children.splice(index, 0, componentConfig);
    } else {
      index = this.config.children.length;
      componentConfig.sortOrder = index;
      this.config.children.push(componentConfig);
    }

    if (this.canRender(componentConfig)) {
      this.addComponent(componentConfig, index);
    }

    this.sortContainers();
  }

  /**
   * A function to be extended by other classes.  This class and something like a LiNavComponent don't have any containers
   * so components can't be added to them.  In other cases, the UlNavComponent has a single container so it can always
   * return that container regardless of the containerName requested (should be "default").  Any component should have
   * a container that is the "default".  For example, a header component might have a container for the left components
   * and a container for the right components.  The left container can just be called "default" and the right container
   * could be called "rightContainer".  If a child component doesn't specify a container, it will be added to the default.
   *
   * @param container
   * @returns {undefined}
   */
  getContainer(container?: string): ViewContainerRef {
    return undefined;
  }

  getComponent(id: string): NavComponent {
    return this.navigationService.getComponent(id);
  }

  getComponentByType(type: string): NavComponent {
    return this.navigationService.getComponentByType(type);
  }

  public setParent(parent: NavComponent) {
    this.parent = parent;
  }

  public getParent(): NavComponent {
    return this.parent;
  }

  public setTitle(title: string) {
    this.title = title;
  }

  setConfig(config) {
    if (isDevMode()) {
      console.debug("setConfig: " + this.initialized);
    }

    if (config) {
      if (config.rendered === undefined) {
        config.rendered = true;
      }

      this.config = config;
      if (this.initialized) {
        this.updateConfig(this.config);
      }
    } else {
      this.config = {
        rendered: true
      };
    }
  }

  /**
   * A function to be extended by other classes.  The children should call super.updateConfig(config).  This sets the
   * common properties of the NavComponent.
   *
   * @param config
   */
  updateConfig(config) {
    if (config.type) {
      this.type = config.type;
    }
    if (this.type) {
      if (typeof this.type === "string") {
        this.typeName = this.type;
      }
      // The componentType decorator was created for uglification. Use it!
      else if (this.type["componentType"]) {
        this.typeName = this.type["componentType"];
      }
      else {
        this.typeName = (<Type<any>>this.type).name;
      }
    }

    if (config.id) {
      this.id = config.id;
    } else if (!this.id) {
      this.id = this.navigationService.getIdNavComponent();
    }
    this.setId();

    if (this.isRoot) {
      this.navigationService.setRootComponent(this.id, this);
    }

    if (isDevMode()) {
      console.debug("NavComponent.updateConfig: " + this.id + ": " + this.initialized);
    }

    if (config.authenticatedKey) {
      this.authenticatedKey = config.authenticatedKey;
    }
    if (config.notAuthenticatedKey) {
      this.notAuthenticatedKey = config.notAuthenticatedKey;
    }
    if (config.roleName) {
      this.roleName = config.roleName;
    }
    if (config.title) {
      this.title = config.title;
    }
    if (config.rootClass) {
      this.rootClass = config.rootClass;
      this.setRootClass();
    }
    if (config.children) {
      this.inputChildren = config.children;
      this.setChildren();
    }

    this.detectChanges();

    if (isDevMode()) {
      console.debug("NavComponent.updateConfig: " + (this.constructor.name) + ", " + this.id + ", " + this.navigationService.getId());
    }
  }

  setId(): void {
    this.navigationService.registerComponent(this.id, this);
    this.renderer.setAttribute(this.elementRef.nativeElement, "id", this.id);
  }

  setRootClass(): void {
    if (this.rootClass) {
      let classes: string[] = this.rootClass.split(" ");
      for (let rootClass of classes) {
        this.renderer.addClass(this.elementRef.nativeElement, rootClass);
      }
    }
  }

  setChildren(): void {
    if (this.initialized) {
      this.clear();
    }
    for (let i = 0; i < this.config.children.length; i++) {
      this.config.children[i].sortOrder = i;
      this.add(this.config.children[i], i);
    }
  }

  detectChanges(): void {
    if (!(<ViewRef>this.changeDetectorRef).destroyed) {
      this.changeDetectorRef.detectChanges();
    }
  }
}
