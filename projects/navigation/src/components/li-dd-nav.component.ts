import {
  ChangeDetectorRef,
  Component, ComponentFactoryResolver, ElementRef, Input, Renderer2, ViewChild, ViewContainerRef
} from "@angular/core";

import {NgbDropdown} from "@ng-bootstrap/ng-bootstrap";

import {ComponentType} from "@huntsman-cancer-institute/utils";

import {NavComponent} from "./nav.component";
import {NavigationService} from "../services/navigation.service";

@ComponentType("LiDdNavComponent")
@Component({
  selector: "hci-li-dd",
  template: `
    <li [id]="id + '-li'" class="hci-li-dd-li dropdown {{liClass}}" (click)="handleLiClick($event)" ngbDropdown #dropdown="ngbDropdown" [autoClose]="true">
      <a id="li-dd-dropdown" class="hci-li-dd-a dropdown-toggle {{aClass}}" ngbDropdownToggle>
        <i *ngIf="iClass && !iRight" class="hci-li-dd-i">
          <span [class]="iClass"></span>
        </i>
        <span class="hci-li-exp-span {{spanClass}}">{{title}}</span>
        <i *ngIf="iClass && iRight" class="hci-li-dd-i">
          <span [class]="iClass"></span>
        </i>
      </a>
      <ul ngbDropdownMenu aria-labelledby="li-dd-dropdown" class="hci-li-dd-ul dropdown-menu {{ulClass}}" (click)="close()">
        <ng-container #containerRef></ng-container>
      </ul>
    </li>
  `
})
export class LiDdNavComponent extends NavComponent {

  @ViewChild("dropdown", {static: true}) dropdown: NgbDropdown;

  @Input() liClass: string = null;
  @Input() href: string = null;
  @Input() aClass: string = "nav-link";
  @Input() spanClass: string = null;
  @Input() iClass: string = null;
  @Input() ulClass: string = "dropdown-menu";
  @Input() iRight: boolean = false;
  @Input() liClick: Function = (() => {});

  private containerRef: ViewContainerRef;

  private container: NavComponent[] = [];

  constructor(elementRef: ElementRef, renderer: Renderer2, resolver: ComponentFactoryResolver, navigationService: NavigationService, changeDetectorRef: ChangeDetectorRef) {
    super(elementRef, renderer, resolver, navigationService, changeDetectorRef);
  }

  @ViewChild("containerRef", {read: ViewContainerRef, static: true}) set containerRefSetter(containerRef: ViewContainerRef) {
    this.containerRef = containerRef;
    this.checkInitialized();
  }

  checkInitialized() {
    if (this.afterViewInit && this.containerRef) {
      this.initialized = true;
      this.updateConfig(this.config);
      this.changeDetectorRef.detectChanges();
    }
  }

  close(): void {
    this.dropdown.close();
  }

  getContainer(container: string): ViewContainerRef {
    return this.containerRef;
  }

  getDropdown(): NgbDropdown {
    return this.dropdown;
  }

  handleLiClick(event: Event) {
    this.liClick(this, this.navigationService, event);
  }

  setConfig(config) {
    this.rootClass = "nav-item";

    super.setConfig(config);
  }

  updateConfig(config) {
    super.updateConfig(config);

    if (config.liClass) {
      this.liClass = config.liClass;
    }
    if (config.href) {
      this.href = config.href;
    }
    if (config.aClass) {
      this.aClass = config.aClass;
    }
    if (config.spanClass) {
      this.spanClass = config.spanClass;
    }
    if (config.iClass) {
      if (this.iClass !== config.iClass) {
        this.iClass = undefined;
        this.detectChanges();
      }
      this.iClass = config.iClass;
    }
    if (config.iRight !== undefined) {
      this.iRight = config.iRight;
    }
    if (config.ulClass) {
      this.ulClass = config.ulClass;
    }
    if (config.liClick) {
      this.liClick = config.liClick;
    }
  }
}
