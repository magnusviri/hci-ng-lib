import {
  ChangeDetectorRef,
  Component, ComponentFactoryResolver, ElementRef, Input, isDevMode, Renderer2, SimpleChange, SimpleChanges,
  TemplateRef, Type, ViewChild, ViewContainerRef
} from "@angular/core";

import {ComponentInjector, ComponentType} from "@huntsman-cancer-institute/utils";

import {NavComponent} from "./nav.component";
import {NavigationService} from "../services/navigation.service";

/**
 * <p>
 *   This component may take a template defined in the parent and display it.  Any information it wishes to use must be
 *   passed with the config.
 * </p>
 * <p>
 *    See demo/src/app/template/template.component.ts for an implementation example.
 * </p>
 *
 */
@ComponentType("TemplateComponent")
@Component({
  selector: "hci-template",
  template: `
    <ng-container [ngTemplateOutlet]="template" [ngTemplateOutletContext]="config"></ng-container>
    <ng-container #viewContainer></ng-container>
  `
})
export class TemplateComponent extends NavComponent {

  @Input() template: TemplateRef<any>;
  @Input() componentType: Type<any> | string;

  component: any;

  @ViewChild("viewContainer", { read: ViewContainerRef, static: false }) private viewContainer: ViewContainerRef;

  constructor(elementRef: ElementRef, renderer: Renderer2, resolver: ComponentFactoryResolver, navigationService: NavigationService, changeDetectorRef: ChangeDetectorRef) {
    super(elementRef, renderer, resolver, navigationService, changeDetectorRef);
  }

  setConfig(config) {
    super.setConfig(config);
  }

  updateConfig(config) {
    if (!config) {
      return;
    }

    if (config.template) {
      this.template = config.template;
    }
    if (config.componentType) {
      // Only allow component if there is no template
      this.componentType = config.componentType;
      this.component = ComponentInjector.getComponent(this.resolver, this.viewContainer, this.componentType);
    }

    this.setComponentConfig();

    super.updateConfig(config);
  }

  setComponentConfig() {
    if (this.component && this.config) {
      let cfgKeys = Object.keys(this.config);
      if (isDevMode()) {
        console.debug("componentConfig: " + JSON.stringify(cfgKeys));
      }
      for (let cfgKey of cfgKeys) {
        try {
          let changes = {};
          changes[cfgKey] = new SimpleChange(this.component[cfgKey], this.config[cfgKey], false);
          this.component.ngOnChanges(changes);
        } catch (e) {}
      }
    }
  }
}
