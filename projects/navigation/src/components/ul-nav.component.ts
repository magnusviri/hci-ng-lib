import {
  ChangeDetectorRef,
  Component, ComponentFactoryResolver, ElementRef, Input, Renderer2, ViewChild, ViewContainerRef
} from "@angular/core";

import {ComponentType} from "@huntsman-cancer-institute/utils";

import {NavComponent} from "./nav.component";
import {NavigationService} from "../services/navigation.service";

@ComponentType("UlNavComponent")
@Component(
  {
    selector: "hci-ul",
    template:
      `
        <ul [id]="id + '-ul'" class="hci-ul {{ulClass}}">
          <ng-container #containerRef></ng-container>
        </ul>
      `,
    styles: [
      `
        .hci-dic-ul-class {
          color: white;
          background-color: var(--greywarm-meddark);
        }
      `
    ]
  }
)
export class UlNavComponent extends NavComponent {

  @Input() ulClass: string = "nav-container";

  containerRef: ViewContainerRef;

  private container: NavComponent[] = [];

  constructor(
    elementRef: ElementRef,
    renderer: Renderer2,
    resolver: ComponentFactoryResolver,
    navigationService: NavigationService,
    changeDetectorRef: ChangeDetectorRef
  ) {
    super(elementRef, renderer, resolver, navigationService, changeDetectorRef);
  }

  @ViewChild("containerRef", {
    read: ViewContainerRef,
    static: true
  }) set containerRefSetter(containerRef: ViewContainerRef) {
    this.containerRef = containerRef;
    this.checkInitialized();
  }

  checkInitialized() {
    if (this.afterViewInit && this.containerRef) {
      this.initialized = true;
      this.updateConfig(this.config);
      this.changeDetectorRef.detectChanges();
    }
  }

  getContainer(container?: string): ViewContainerRef {
    return this.containerRef;
  }

  setConfig(config): void {
    super.setConfig(config);
  }

  updateConfig(config): void {
    super.updateConfig(config);

    if (config.ulClass) {
      this.ulClass = config.ulClass;
    }
  }
}
