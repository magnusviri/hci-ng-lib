/*
 * Copyright (c) 2016 Huntsman Cancer Institute at the University of Utah, Confidential and Proprietary
 */
import {
  ChangeDetectorRef,
  Component, ComponentFactoryResolver, ElementRef, HostBinding, Input, Renderer2, ViewChild,
  ViewContainerRef
} from "@angular/core";

import {NavigationService} from "../services/navigation.service";
import {NavComponent} from "../components/nav.component";
import {NavigationGlobalService} from "../services/navigation-global.service";


/**
 * A component to provide an application wide fixed footer exposing application name, version, a navigation element to a
 * feedback form and a navigation element to contact information.
 * <p>
 * This component expects bootstrap 4.x styling to be applied.
 *
 * @since 1.0.0
 */
@Component({
  selector: "hci-app-footer",
  template: `
    <div class="hci-app-footer {{ footerClasses ? ' ' + footerClasses : '' }}">
      <div id="footer-left-container" class="ml-3">
        <ng-container #leftContainer></ng-container>
      </div>
      <div id="footer-middle-container" class="ml-auto mr-auto">
        <ng-container #middleContainer></ng-container>
      </div>
      <div id="footer-right-container" class="mr-3">
        <ng-container #rightContainer></ng-container>
      </div>
    </div>
  `,
  providers: [
    NavigationService
  ]
})
export class AppFooterComponent extends NavComponent {

  @HostBinding("class") classList: String = "flex-shrink-0";

  @Input() footerClasses: string = "d-none d-md-flex";

  navigationGlobalService: NavigationGlobalService;

  leftContainer: ViewContainerRef;
  middleContainer: ViewContainerRef;
  rightContainer: ViewContainerRef;

  constructor(elementRef: ElementRef,
              renderer: Renderer2,
              resolver: ComponentFactoryResolver,
              navigationGlobalService: NavigationGlobalService,
              navigationService: NavigationService,
              changeDetectorRef: ChangeDetectorRef) {
    super(elementRef, renderer, resolver, navigationService, changeDetectorRef);

    this.isRoot = true;
    this.navigationGlobalService = navigationGlobalService;
    this.containerSet.add("container");
  }

  @ViewChild("leftContainer", {read: ViewContainerRef, static: true}) set leftContainerSetter(leftContainer: ViewContainerRef) {
    this.leftContainer = leftContainer;
    this.checkInitialized();
  }

  @ViewChild("middleContainer", {read: ViewContainerRef, static: true}) set middleContainerSetter(middleContainer: ViewContainerRef) {
    this.middleContainer = middleContainer;
    this.checkInitialized();
  }

  @ViewChild("rightContainer", {read: ViewContainerRef, static: true}) set rightContainerSetter(rightContainer: ViewContainerRef) {
    this.rightContainer = rightContainer;
    this.checkInitialized();
  }

  checkInitialized() {
    if (this.afterViewInit && this.leftContainer && this.middleContainer && this.rightContainer) {
      this.initialized = true;
      this.updateConfig(this.config);
      this.changeDetectorRef.detectChanges();
    }
  }

  clear() {
    super.clear();

    if (this.leftContainer) {
      this.leftContainer.clear();
    }
    if (this.middleContainer) {
      this.middleContainer.clear();
    }
    if (this.rightContainer) {
      this.rightContainer.clear();
    }

    this.changeDetectorRef.detectChanges();
  }

  getContainer(containerName?: string): ViewContainerRef {
    if (containerName === undefined || containerName === "default") {
      return this.middleContainer;
    } else if (containerName === "leftContainer") {
      return this.leftContainer;
    } else {
      return this.rightContainer;
    }
  }

  updateConfig(config) {
    if (config.navbarClasses) {
      this.footerClasses = config.footerClasses;
    }

    super.updateConfig(config);
  }

}
