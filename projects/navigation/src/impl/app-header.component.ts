/*
 * Copyright (c) 2016 Huntsman Cancer Institute at the University of Utah, Confidential and Proprietary
 */
import {
  ChangeDetectorRef,
  Component, ComponentFactoryResolver, ElementRef, HostBinding, Input, Renderer2, ViewChild, ViewContainerRef
} from "@angular/core";

import {NavigationService} from "../services/navigation.service";
import {NavComponent} from "../components/nav.component";
import {NavigationGlobalService} from "../services/navigation-global.service";

/**
 * A component intended to provide a common, configurable application header. This component expects to be used with a
 * ViewChild pattern. See the demo application and associated documentation for examples and details.
 *
 * @since 1.0.0
 */
@Component({
  selector: "hci-app-header",
  template: `
    <nav [id]="id + '-nav'"
         class="navbar navbar-expand-md navbar-dark {{ navbarClasses ? ' ' + navbarClasses : '' }}"
         [style.display]="hidden ? 'none' : 'flex'">
      <div id="leftHeaderContainer" class="mr-auto left-header">
        <ng-container #leftContainer></ng-container>
      </div>
      <div id="rightHeaderContainer" class="d-none d-md-flex right-header">
        <ng-container #rightContainer></ng-container>
      </div>
    </nav>
  `,
  providers: [
    NavigationService
  ]
})
export class AppHeaderComponent extends NavComponent {

  @HostBinding("class") @Input("class") classList: String = "";

  @Input() hidden: boolean = false;
  @Input() navbarClasses: string;

  navigationGlobalService: NavigationGlobalService;

  leftContainer: ViewContainerRef;
  rightContainer: ViewContainerRef;

  constructor(elementRef: ElementRef,
              renderer: Renderer2,
              resolver: ComponentFactoryResolver,
              navigationGlobalService: NavigationGlobalService,
              navigationService: NavigationService,
              changeDetectorRef: ChangeDetectorRef) {
    super(elementRef, renderer, resolver, navigationService, changeDetectorRef);


    this.isRoot = true;
    this.navigationGlobalService = navigationGlobalService;
    this.containerSet.add("rightContainer");

    if(this.classList.length > 0) {
      this.classList += " flex-shrink-0";
    }else {
      this.classList = "flex-shrink-0";
    }
  }

  @ViewChild("leftContainer", {read: ViewContainerRef, static: true}) set leftContainerSetter(leftContainer: ViewContainerRef) {
    this.leftContainer = leftContainer;
    this.checkInitialized();
  }

  @ViewChild("rightContainer", {read: ViewContainerRef, static: true}) set rightContainerSetter(rightContainer: ViewContainerRef) {
    this.rightContainer = rightContainer;
    this.checkInitialized();
  }

  checkInitialized() {
    if (this.afterViewInit && this.leftContainer && this.rightContainer) {
      this.initialized = true;
      this.updateConfig(this.config);
      this.changeDetectorRef.detectChanges();
    }
  }

  clear() {
    super.clear();

    if (this.leftContainer) {
      this.leftContainer.clear();
    }
    if (this.rightContainer) {
      this.rightContainer.clear();
    }

    this.changeDetectorRef.detectChanges();
  }

  getContainer(containerName?: string): ViewContainerRef {
    if (containerName === undefined || containerName === "default") {
      return this.leftContainer;
    } else {
      return this.rightContainer;
    }
  }

  setConfig(config: any) {
    super.setConfig(config);
  }

  updateConfig(config: any) {
    if (config.navbarClasses) {
      this.navbarClasses = config.navbarClasses;
    }
    if (config.hidden !== undefined) {
      this.hidden = config.hidden;
    }

    super.updateConfig(config);
  }

}
