import {Widget} from "./widget.entity";
import {WidgetAttributeValueSet} from "./widget-attribute-value-set.entity";

export class WidgetInstance {
  public idWidgetInstance: number;
  public idDashboard: number;
  public widget: Widget;
  public sortOrder: number;
  public attributeValueSet: WidgetAttributeValueSet;

  deserialize(object: any): WidgetInstance {
    this.idWidgetInstance = object.idWidgetInstance;
    this.idDashboard = object.idDashboard;
    this.sortOrder = object.sortOrder;

    if (object.widget) {
      this.widget = new Widget().deserialize(object.widget);
    }
    if (object.attributeValueSet) {
      this.attributeValueSet = new WidgetAttributeValueSet().deserialize(object.attributeValueSet);
    }

    return this;
  }

  idReset() {
    if (this.idWidgetInstance < 0) {
      this.idWidgetInstance = undefined;
    }

    if (this.attributeValueSet) {
      this.attributeValueSet.idReset();
    }
  }

}
