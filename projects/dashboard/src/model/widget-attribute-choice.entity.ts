import {WidgetAttribute} from "./widget-attribute.entity";

export class WidgetAttributeChoice {
  public idWidgetAttributeChoice: number;
  public value: string;
  public display: string;

  deserialize(object): WidgetAttributeChoice {
    this.idWidgetAttributeChoice = object.idWidgetAttributeChoice;
    this.value = object.value;
    this.display = object.display;

    return this;
  }

  deserializeArray(list: Object[]): WidgetAttributeChoice[] {
    let choices: WidgetAttributeChoice[] = [];
    for (var i = 0; i < list.length; i++) {
      choices.push(new WidgetAttributeChoice().deserialize(list[i]));
    }
    return choices;
  }
}
