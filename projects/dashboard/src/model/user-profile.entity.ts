import {Dashboard} from "./dashboard.entity";

export class UserProfile {
  public idUserProfile: number;
  public idUser: number;
  public dashboards: Dashboard[];

  deserialize(object): UserProfile {
    this.idUserProfile = object.idUserProfile;
    this.idUser = object.idUser;

    if (object.dashboards) {
      this.dashboards = [];
      for (var i = 0; i < object.dashboards.length; i++) {
        this.dashboards.push(new Dashboard().deserialize(object.dashboards[i]));
      }
    }

    return this;
  }
}
