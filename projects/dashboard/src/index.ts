/*
 * Copyright (c) 2016 Huntsman Cancer Institute at the University of Utah, Confidential and Proprietary
 */
/**
 * A barrel file for the HCI ng dashboard package.
 *
 * @since 1.0.0
 */
export {DashboardModule} from "./dashboard.module";
export {DashboardComponent} from "./dashboard.component";
export {DashboardService, USER_PROFILE_ENDPOINT, DASHBOARD_TYPE} from "./services/dashboard.service";
export {DashboardGlobalService} from "./services/dashboard-global.service";
export {WidgetHostComponent} from "./widget/widget-host.component";
export {WidgetComponent} from "./widget/widget.component";
export {WidgetContainerComponent} from "./widget/widget-container.component";
export {WidgetAddComponent} from "./configure/widget-add.component";
export {WidgetConfigureComponent} from "./configure/widget-configure.component";
export {WidgetAttributeComponent} from "./configure/widget-attribute.component";

export {Dashboard} from "./model/dashboard.entity";
export {UserProfile} from "./model/user-profile.entity";
export {Widget} from "./model/widget.entity";
export {WidgetAttribute} from "./model/widget-attribute.entity";
export {WidgetAttributeChoice} from "./model/widget-attribute-choice.entity";
export {WidgetAttributeContainer} from "./model/widget-attribute-container.entity";
export {WidgetAttributeValue} from "./model/widget-attribute-value.entity";
export {WidgetAttributeValueSet} from "./model/widget-attribute-value-set.entity";
export {WidgetCategory} from "./model/widget-category.entity";
export {WidgetInstance} from "./model/widget-instance.entity";
