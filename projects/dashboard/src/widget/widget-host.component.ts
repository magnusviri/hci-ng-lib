/*
 * Copyright (c) 2016 Huntsman Cancer Institute at the University of Utah, Confidential and Proprietary
 */
import {Component, ComponentFactoryResolver, ElementRef, Input, isDevMode, Renderer2, Type, ViewChild, ViewContainerRef} from "@angular/core";

import {ComponentInjector} from "@huntsman-cancer-institute/utils";

import {WidgetInstance} from "../model/model";
import {WidgetComponent} from "./widget.component";
import {DashboardService} from "../services/dashboard.service";

/**
 * Based upon the main configuration property which is { widget: "WidgetComponent" }, this will dynamically inject
 * an instance of that widget based on its class name.
 *
 * @since 1.0.0
 */
@Component({
  selector: "widget-host",
  template: `
    <div class="widget-host card-padding" (dblclick)="openConfiguration()">
      <ng-container #widgetComponent></ng-container>
    </div>
  `,
  styles: [`

    .card-padding {
      padding: .75rem;
      border: 1px solid rgba(0,0,0,.125);
      height: 100%;
      background-color: #f8f8fc;
    }

    .widget-right-corner {
      float: right;
      margin-right: 0px;
      margin-left: auto;
    }
    .widget-right-corner.next {
      padding-right: 10px;
    }
    .widget-right-corner.light {
      color: #dddddd;
    }
    .no-dropdown-icon.dropdown-toggle::after {
      display: none;
    }
    .dropdown-menu {
      top: 0 !important;
      margin-top: -2px;
    }
    .dropdown-menu.submenu {
      padding-left: 15px;
      border: none;
      position: relative;
    }
  `]
})
export class WidgetHostComponent {

  @Input() widgetInstance: WidgetInstance;

  public editMode: boolean = false;
  public showIcons: boolean = false;
  public sizeCollapsed: boolean = true;
  public isFullscreen: boolean = false;

  public widgetComponent: WidgetComponent;

  configMode: boolean = false;

  @ViewChild("widgetComponent", { read: ViewContainerRef, static: true })
  private widgetHolderRef: any;

  constructor(private renderer: Renderer2, private elementRef: ElementRef, private resolver: ComponentFactoryResolver, private dashboardService: DashboardService) {}

  ngOnInit() {
    this.createWidget();

    this.dashboardService.getEditMode().subscribe((editMode: boolean) => {
      this.editMode = editMode;
    });

    this.dashboardService.configurationWidgetInstanceSubject.subscribe((widgetInstance: WidgetInstance) => {
      if (!widgetInstance) {
        this.setConfigMode(false);
      } else if (widgetInstance.idWidgetInstance === this.widgetInstance.idWidgetInstance) {
        this.setConfigMode(true);
      } else {
        this.setConfigMode(false);
      }
    });

    this.dashboardService.updateWidgetInstanceSubject.subscribe((widgetInstance: WidgetInstance) => {
      if (widgetInstance.idWidgetInstance === this.widgetInstance.idWidgetInstance) {
        this.widgetInstance = widgetInstance;
        this.configureComponentRef();
      }
    });
  }

  setConfigMode(configMode: boolean) {
    this.configMode = configMode;

    if (this.configMode) {
      this.renderer.setStyle(this.elementRef.nativeElement, "z-index", "8");
      this.renderer.setStyle(this.elementRef.nativeElement, "background-color", "rgba(255, 0, 0, 0.25)");
    } else {
      this.renderer.removeStyle(this.elementRef.nativeElement, "z-index");
      this.renderer.removeStyle(this.elementRef.nativeElement, "background-color");
    }
  }

  /**
   * Create the widget based upon the widget's specified component.
   */
  createWidget() {
    if (this.widgetInstance.widget) {
      let factories: Type<any>[];

      try {
        if (isDevMode()) {
          console.debug("createWidget: " + this.widgetInstance.widget.component);
          console.debug(this.widgetInstance);
        }

        this.widgetComponent = ComponentInjector.getComponent(this.resolver, this.widgetHolderRef, this.widgetInstance.widget.component);

        this.dashboardService.registerWidgetComponent(this.widgetInstance.idWidgetInstance, this.widgetComponent);

        this.configureComponentRef();
      } catch (e) {
        let widgetName: string = (this.widgetInstance) ? ((this.widgetInstance.widget) ? this.widgetInstance.widget.component : "Undefined Widget") : "Undefined WidgetInstance";
        console.error("createWidget for '" + widgetName + "'", e);
      }
    }
  }

  /**
   * TODO: Update actual attribute value.  If one doesn't exist, create it.
   *
   * @param cardClass
   */
  setSize(cardClass: string) {
    this.renderer.setAttribute(this.elementRef.nativeElement, "class", "");
    this.renderer.addClass(this.elementRef.nativeElement, "card");
    this.renderer.addClass(this.elementRef.nativeElement, cardClass);
  }

  /**
   * Notify any service listeners that we want to open a user configuration window on this widget.
   */
  openConfiguration() {
    if (!this.editMode) {
      return;
    }

    this.dashboardService.setConfigurationWidgetInstance(this.widgetInstance, this.widgetComponent, false);
  }

  configureComponentRef() {
    this.widgetComponent.setWidgetInstance(this.widgetInstance);
    this.widgetComponent.config(undefined);
    if (this.widgetComponent.getCardClass() && this.widgetComponent.getCardClass().length > 0) {
      this.renderer.setAttribute(this.elementRef.nativeElement, "class", "");
      this.renderer.addClass(this.elementRef.nativeElement, "card");
      this.renderer.addClass(this.elementRef.nativeElement, this.widgetComponent.getCardClass());
    }
    this.widgetComponent.cardClassEvent.subscribe((cardClass: string) => {
      this.setSize(cardClass);
    });
  }

  deleteWidget() {
    this.dashboardService.deleteWidget(this.widgetInstance);
  }

  fullscreenOpen() {
    this.isFullscreen = true;
    this.showIcons = false;
    this.renderer.addClass(this.elementRef.nativeElement, "widget-fullscreen");
  }

  fullscreenClose() {
    this.isFullscreen = false;
    this.showIcons = false;
    this.renderer.removeClass(this.elementRef.nativeElement, "widget-fullscreen");
  }
}
