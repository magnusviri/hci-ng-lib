
export {DictionaryServiceModule} from "./dictionary-service.module";
export {DictionaryService} from "./services/dictionary.service";
export {DropdownEntry} from "./model/dropdown-entry.dto";

export {DICTIONARY_ENDPOINT} from "./services/dictionary.service";
