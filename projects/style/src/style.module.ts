/*
 * Copyright (c) 2016 Huntsman Cancer Institute at the University of Utah, Confidential and Proprietary
 */
import {ModuleWithProviders, NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {StyleComponent} from './style.component';


/**
 * A feature module for user related services, directives, pipes, etc...
 *
 * @since 1.0.0
 */
@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [
    StyleComponent
  ],
  exports: [
    StyleComponent
  ]
})
export class StyleModule {
  static forRoot(): ModuleWithProviders<StyleModule> {
    return {
      providers: [
      ],
      ngModule: StyleModule
    };
  }
}
