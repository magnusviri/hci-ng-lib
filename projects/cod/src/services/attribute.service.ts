import {Inject, Injectable, InjectionToken, isDevMode} from "@angular/core";
import {HttpClient, HttpParams} from "@angular/common/http";
import {Router} from "@angular/router";

import {BehaviorSubject, forkJoin, Observable, Subject} from "rxjs";

import {DictionaryService} from "@huntsman-cancer-institute/dictionary-service";

import {Attribute} from "../model/attribute.entity";
import {AttributeChoice} from "../model/attribute-choice.entity";
import {AttributeValue} from "../model/attribute-value.entity";
import {AttributeConfiguration} from "../model/attribute-configuration.entity";
import {AttributeValueSet} from "../model/attribute-value-set.entity";
import {AttributeConfigurationDTO} from "../model/attribute-configuration.dto";
import {GraphicalAttribute} from "../model/graphical-attribute.entity";
import {AttributeContainer} from "../model/attribute-container.entity";
import {AttributeValueGridRow} from "../model/attribute-value-grid-row.entity";
import {PreEvalDTO} from "../model/pre-eval.dto";
import {DictionaryEntriesDTO} from "../model/dictionary-entries.dto";


export let ATTRIBUTE_ENDPOINT = new InjectionToken<string>("attributeEndpoint");
export const BOOLEAN: string[] = ["N", "Y"];
export const EXTENDED_BOOLEAN: string[] = ["N", "Y", "U"];

/**
 * A service created for each attribute configuration instance.  This makes calls to the backend to get the configuration
 * and value set.  It also stores common data that different attributes might need (e.g. a list of attribute choices).
 */
@Injectable()
export class AttributeService {

  newAttributeConfiguration: AttributeConfiguration;
  idAttributeConfiguration: number;
  idFilter1: number;
  idFilter2: number;
  idAttributeValueSet: number;
  idParentObject: number;
  boundData: any;

  attributeContextLoadingSubject: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);

  attributeContextListSubject: BehaviorSubject<any[]> = new BehaviorSubject<any[]>(undefined);
  attributeSecurityContextListSubject: BehaviorSubject<any[]> = new BehaviorSubject<any[]>(undefined);
  filter1ListSubject: BehaviorSubject<any[]> = new BehaviorSubject<any[]>(undefined);
  filter2ListSubject: BehaviorSubject<any[]> = new BehaviorSubject<any[]>(undefined);
  dictionaryListSubject: BehaviorSubject<any[]> = new BehaviorSubject<any[]>(undefined);


  postConfigurationSubject: Subject<boolean> = new Subject<boolean>();
  attributeConfigurationDimensionSubject: BehaviorSubject<any> = new BehaviorSubject<any>(undefined);

  attributeConfigurationListSubject: BehaviorSubject<AttributeConfigurationDTO[]> = new BehaviorSubject<AttributeConfigurationDTO[]>(undefined);
  attributeConfigurationListLoadingSubject: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);

  attributeConfigurationDTOSubject: BehaviorSubject<AttributeConfigurationDTO> = new BehaviorSubject<AttributeConfigurationDTO>(undefined);

  attributeConfigurationSubject: BehaviorSubject<AttributeConfiguration> = new BehaviorSubject<AttributeConfiguration>(undefined);
  attributeValueSetSubject: BehaviorSubject<AttributeValueSet> = new BehaviorSubject<AttributeValueSet>(undefined);
  attributeValuePushedSubject: Subject<AttributeValue> = new Subject<AttributeValue>();

  loadingSubject: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);

  attributeMap: Map<number, Attribute> = new Map<number, Attribute>();

  width: number;

  containerUpdated: Subject<boolean> = new Subject<boolean>();
  gridRowSaved: Subject<AttributeValueGridRow> = new Subject<AttributeValueGridRow>();
  gridRowDeleted: Subject<AttributeValueGridRow> = new Subject<AttributeValueGridRow>();
  dirty: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  updatedAttributeValues: AttributeValue[] = [];
  slicedAttributeValues: AttributeValue[] = [];

  private uniqueId: number = 0;
  private _editButtonsEnabled: boolean = true; //Preserve old default

  constructor(private dictionaryService: DictionaryService,
              private http: HttpClient,
              private router: Router,
              @Inject(ATTRIBUTE_ENDPOINT) private attributeEndpoint: string) {
    if (isDevMode()) {
      console.debug("Created New AttributeService");
    }
  }

  setBoundData(boundData: any) {
    this.boundData = boundData;

    if (this.attributeValueSetSubject.getValue()) {
      this.notifyAttributes();
    }
  }

  public get editButtonsEnabled(): boolean {
    return this._editButtonsEnabled;
  };

  public set editButtonsEnabled(enabled: boolean) {
    if (enabled !== this._editButtonsEnabled) {
      this._editButtonsEnabled = enabled;
      this.notifyAttributes();
    }
  }

  getAttributeValueCountByAttribute(idAttribute: number): Observable<number> {
    return this.http.get<number>(this.attributeEndpoint + "attribute-value-count", {params: new HttpParams().set("idAttribute", idAttribute.toString())});
  }

  getAttributeValueCountByContainer(idAttributeContainer: number): Observable<number> {
    return this.http.get<number>(this.attributeEndpoint + "attribute-value-count", {params: new HttpParams().set("idAttributeContainer", idAttributeContainer.toString())});
  }

  getDictionaryValueCount(idAttributeDictionary: number): Observable<number> {
    return this.http.get<number>(this.attributeEndpoint + "attribute-dictionary-count", {params: new HttpParams().set("idAttributeDictionary", idAttributeDictionary.toString())});
  }

  fetchAttributeContext(): void {
    this.attributeContextLoadingSubject.next(true);

    forkJoin([
      this.dictionaryService.getDictionaryEntries("hci.ri.attr.model.AttributeSecurityContext"),
      this.dictionaryService.getDictionaryEntries("hci.ri.attr.model.AttributeContext"),
      this.dictionaryService.getDictionaryEntries("hci.ri.attr.model.Filter1"),
      this.dictionaryService.getDictionaryEntries("hci.ri.attr.model.Filter2"),
      this.dictionaryService.getDictionaryEntries("hci.ri.attr.model.AttributeDictionary")
    ]).subscribe((responses: any[]) => {

      this.dictionaryListSubject.next(responses[4]);
      this.filter1ListSubject.next(responses[2]);
      this.filter2ListSubject.next(responses[3]);
      this.attributeContextListSubject.next(responses[1]);

      responses[0].sort(this.sortAttributeSecurityContextList);
      this.attributeSecurityContextListSubject.next(responses[0]);

      this.attributeContextLoadingSubject.next(false);
    });
  }

  //Gets the dictionary entries from the map (so we don't load over and over and over for the same dictionary)
  getDictionaryEntries(attribute: Attribute, values: AttributeValue[], groupAttributeRowId?: number, referencedAttributeValue?: AttributeValue): Observable<DictionaryEntriesDTO> {
    let bs = new Subject<DictionaryEntriesDTO>();

    //Handle XPATH
    if (attribute.attributeDictionary.xpath) {
      let entriesDTO = new DictionaryEntriesDTO();
      let xpath = attribute.attributeDictionary.xpath;

      //Filtered by another dictionary? Parse to get array of all dictionary class names needed
      let dictClasses: string[] = [];
      let pos = 0;
      while (xpath.indexOf("/Dictionaries/Dictionary", pos) >= 0) {
        let start = xpath.indexOf("/Dictionaries/Dictionary", pos);
        start = xpath.indexOf("'", start);
        let end = xpath.indexOf("'", start + 1);
        dictClasses.push(attribute.attributeDictionary.xpath.substring(start + 1, end))
        pos = end;
      }

      //Potentially load multiple dictionaries
      let forkCalls = [];
      for (let className of dictClasses) {
        forkCalls.push(this.dictionaryService.getDictionaryEntriesAsXML(className));
      }

      //Deal with the results together
      forkJoin(forkCalls).subscribe((responses: any[]) => {
        //We're only going to do xpath on the xml for the first dictionary
        const parser = new DOMParser();
        let doc = parser.parseFromString(responses[0], "application/xml");

        //Have to evaluate sub-expression. This doesn't actually support expressions like [@x = (/xpath-sub-expression)]
        let secondaryXml = undefined;
        if (responses.length > 1) {
          secondaryXml = responses[1];
        }

        //Pre-evaluate and simplify XPATH
        let dto:PreEvalDTO = this.preEvaluateXpath(attribute.attributeDictionary.xpath, secondaryXml, groupAttributeRowId, referencedAttributeValue);

        //Evaluate XPATH expression
        let entries = [];
        entriesDTO.entriesIncludeSelectedValue = false;

        try{
          let xPathResult = document.evaluate(dto.expression, doc, null, XPathResult.ANY_TYPE, null);

          //Iterate xpath results (nodes)
          var node = null;
          while(node = xPathResult.iterateNext()) {

            let entry = this.getJsonEntryFromNode(node);
            entries.push(entry);
          }
        }catch (e) {
          // No matching entries here.
        }

        //What about dictionary entries where isActive='N', but the id was used in a previously saved value?
        //We add that value to the dropdown, in that case, so it can be selected
        //ReferencedAttributeValue only gets passed in when the filter attribute changes. In that scenario we do not want to add the old value to the list
        if (attribute.isMultiValue !== "Y") {
          for (let av of values) {
            let present:boolean = false;

            if (av && ! referencedAttributeValue) {
              for(var i = entries.length - 1; i >= 0; i--) {
                if (entries[i].value === av.valueIdDictionary) {
                  present = true;
                  break;
                }
              }

              //If the currently saved value is not in the list, add it.
              if (! present) {

                let xpath = "/Dictionaries/Dictionary[@className='" + attribute.attributeDictionary.className + "']/DictionaryEntry[@value='" + av.valueIdDictionary + "']";
                //The entry was probably already loaded, but excluded by the xpath. We can use that.

                let node = document.evaluate(xpath, doc, null, XPathResult.ANY_UNORDERED_NODE_TYPE, null).singleNodeValue;
                if (node) {
                  entries.push(this.getJsonEntryFromNode(node));
                  entriesDTO.entriesIncludeSelectedValue = true;
                }
              }
            }
          }
        }


        //Set the behavior subject with the filtered values for the dictionary
        entriesDTO.entries = entries;
        entriesDTO.referencedAttribute = dto.referencedAttribute;
        bs.next(entriesDTO);
      });

    }
    //Non-xpath
    else {
      //DictionayService just used http get, so should not need to unsubscribe, since http does that
      this.dictionaryService.getDictionaryDropdownEntries(attribute.attributeDictionary.className).subscribe((entries: any[]) => {
        //Make value fields
        for(var i = entries.length - 1; i >= 0; i--) {
          entries[i].value = entries[i].id.toString();
        }

        let entriesDTO = new DictionaryEntriesDTO();
        entriesDTO.entries = entries;
        entriesDTO.entriesIncludeSelectedValue = true; //A bit of an assumption, but if it isn't filtered, it had better include all the values!
        bs.next(entriesDTO);
      });
    }

    return bs;
  }

  getJsonEntryFromNode(node: any): any {
    //Create JSON objects from the nodes
    let entry: any = {};
    for(var i = node.attributes.length - 1; i >= 0; i--) {
      entry[node.attributes[i].name] = node.attributes[i].value;
    }

    return entry;
  }

  private preEvaluateXpath(xpath: string, secondaryXml: string, groupAttributeRowId: number, referencedAttributeValue: AttributeValue): PreEvalDTO {
    let dto:PreEvalDTO = new PreEvalDTO();

    //SecurityAdvisor/@codeCancerGroup
    while (xpath.indexOf("/SecurityAdvisor/@codeCancerGroup") >= 0) {
      xpath = xpath.replace("/SecurityAdvisor/@codeCancerGroup", "'" + this.getAttributeConfigurationSubject().value.codeAttributeSecurityContext + "'");
    }

    //Replace some of the context parameters ${x.value} - This is more Altio specific stuff that was used in CCR
    let paramPos = xpath.search("\\${[A-Za-z0-9_]+\\.value}");
    while (paramPos >= 0) {
      let prefix = xpath.substring(0, paramPos);
      let varName = xpath.substring(paramPos + 2, xpath.indexOf(".", paramPos));
      let suffix = xpath.substring(xpath.indexOf("}", paramPos) + 1);

      var av;

      //If there IS a referencedAttributeValue, we can use the value directly. Otherwise we try to find it in the AVS
      if (! referencedAttributeValue) {

        //Find the attribute, matching the varName.
        //Do this first, so we get a reference to the related attribute, regardless of whether there is already a value

        //Iterate attributes to find one that matches the value
        this.attributeMap.forEach((attribute: Attribute) => {
          //Attributes that match the name
          if (attribute.attributeName === varName) {
            dto.referencedAttribute = attribute;
            return;
          }

          //If this is a grid, look for attributes in the row
          if (attribute.attributes) {
            for (let gridColumnAttribute  of attribute.attributes) {
              //If the name matches AND the value in the AVS was part of the SAME ROW we are evaluating XPATH for, we can use the value for evaluation
              if (gridColumnAttribute.attributeName === varName) {
                dto.referencedAttribute = gridColumnAttribute;
                return;
              }
            }
          }
        });



        let avs = this.attributeValueSetSubject.getValue();
        //If we found the attribute and have values...
        if (dto.referencedAttribute && avs) {

          //Look through all the values to find the one for the referenced attribute (and SAME grid row if in a grid row)
          av = avs.attributeValues.find((av) => {

            //If this is an AV for a grid row, it must be for the same grid row we are looking for
            if (typeof av.groupAttributeRowId !== 'undefined' && typeof groupAttributeRowId !== 'undefined' && av.groupAttributeRowId !== groupAttributeRowId) {
              return false;
            }


            //If we looked up the attribute matching the value by id AND the name of the attribute is the one we ar looking for from the XPATH, we can use it for evaluation
             //Found an AttributeValue that the dictionary is filtered by
            if (dto.referencedAttribute.idAttribute === av.idAttribute) {
              return true;
            }
            else {
              return false;
            }
          })
        }
      }

      //Use the referencedAttributeValue if provided (this happens when already known, during changes to the referenced attribute)
      if (referencedAttributeValue) {
        xpath = prefix + "'" + referencedAttributeValue.value + "'" + suffix;
      }
      //Found the AttributeValue for the attribute that the dictionary is filtered by
      else if (av) {
        xpath = prefix + "'" + av.value + "'" + suffix;
      }
      //Didn't find an attribute for varName
      else {
        //Maybe the varName is from elsewhere in the app (non-COD), and provided in boundData
        if (this.boundData && this.boundData[varName]) {
          xpath = prefix + "'" + this.boundData[varName] + "'" + suffix;
        }
        //Nothing matches. Change to empty string. Will probably result in NO entries, which would be correct.
        else {
          xpath = prefix + "''" + suffix;
        }
      }

      paramPos = xpath.search("\\${[A-Za-z0-9_]+\\.value}");
    }

    //Have to evaluate sub-expression. This handles expressions like [@x = (/xpath-sub-expression)]
    let inExpressionPos = xpath.search("\\[@[A-Za-z0-9_]+\\s*=\\s*\\(");

    if (inExpressionPos > 0 && secondaryXml) {
      let prefix = xpath.substring(0, inExpressionPos + 1);
      let expressionEnd = xpath.indexOf(")", inExpressionPos);
      let variable = xpath.substring(inExpressionPos + 1, xpath.indexOf("=", inExpressionPos));
      let subExpression = xpath.substring(xpath.indexOf("(", inExpressionPos) + 1, expressionEnd);
      let suffix = xpath.substring(expressionEnd + 1);

      xpath = prefix;

      //Evaluate XPATH expression
      const parser = new DOMParser();
      let doc = parser.parseFromString(secondaryXml, "application/xml");
      let xPathResult = document.evaluate(subExpression, doc, null, XPathResult.ANY_TYPE, null);

      //Iterate xpath results (nodes)
      var node = xPathResult.iterateNext();

      while(node) {
        xpath = xpath + variable + "='" + node.nodeValue + "'"

        node = xPathResult.iterateNext();

        if (node) {
          xpath = xpath + " or ";
        }
      }

      xpath = xpath + suffix;
    }

    dto.expression = xpath;
    return dto;
  }

  private sortAttributeSecurityContextList(a: any, b: any) {
    return a.description.toLowerCase().localeCompare(b.description.toLowerCase());
  }

  fetchAttributeConfigurationList(codeAttributeSecurityContext?: string): void {
    if (isDevMode()) {
      console.debug("fetchAttributeConfigurationList: " + codeAttributeSecurityContext);
    }

    this.attributeConfigurationListLoadingSubject.next(true);

    let url: string = this.attributeEndpoint + "configurations";
    let params: HttpParams = new HttpParams();
    if (codeAttributeSecurityContext) {
      params = params.set("codeAttributeSecurityContext", codeAttributeSecurityContext);
    }

    this.http.get<AttributeConfiguration[]>(url, {params: params}).subscribe((configurationList: AttributeConfiguration[]) => {
      this.attributeConfigurationListLoadingSubject.next(false);

      let dtos: AttributeConfigurationDTO[] = [];
      for (let cfg of configurationList) {
        let dto: AttributeConfigurationDTO = this.getAttributeConfigurationDTO(cfg);
        if (dto) {
          dtos.push(dto);
        }
      }

      dtos.sort(this.sortAttributeConfigurationList);

      this.attributeConfigurationListSubject.next(dtos);
    });
  }

  private sortAttributeConfigurationList(a: AttributeConfigurationDTO, b: AttributeConfigurationDTO) {
    const displaya = a.codeAttributeSecurityContextDisplay
      + a.extensionDescription
      + (a.filter1Display || "")
      + (a.filter2Display || "");

    const displayb = b.codeAttributeSecurityContextDisplay
      + b.extensionDescription
      + (b.filter1Display || "")
      + (b.filter2Display || "");

    return displaya.toLowerCase().localeCompare(displayb.toLowerCase());
  }

  //Used by cod-editor
  clear(): void {
    this.idAttributeConfiguration = undefined;
    this.idFilter1 = undefined;
    this.idFilter2 = undefined;

    this.attributeConfigurationDTOSubject.next(undefined);
    this.attributeConfigurationSubject.next(undefined);
  }

  createAttributeConfiguration(attributeConfiguration: AttributeConfiguration): void {
    attributeConfiguration.idAttributeConfiguration = undefined;

    this.http.post<AttributeConfiguration>(this.attributeEndpoint + "configurations", attributeConfiguration).subscribe((attributeConfiguration: AttributeConfiguration) => {
      this.idAttributeConfiguration = attributeConfiguration.idAttributeConfiguration;

      this.postConfigurationSubject.next(true);
      this.attributeConfigurationSubject.next(attributeConfiguration);
      this.router.navigateByUrl("/editor/attribute-configuration/" + attributeConfiguration.idAttributeConfiguration);
    });
  }

  setAttributeConfigurationById(idAttributeConfiguration: number, idAttributeValueSet: number, idParentObject: number): void {
    if (idAttributeConfiguration !== -1) {
      this.newAttributeConfiguration = undefined;
    }

    //Only load if the AttributeConfiguration has changed
    if (this.idAttributeConfiguration !== idAttributeConfiguration) {
      //Clear ASAP
      this.idAttributeConfiguration = idAttributeConfiguration;
      this.idAttributeValueSet = idAttributeValueSet;
      this.idFilter1 = undefined;
      this.idFilter2 = undefined;
      this.attributeMap.clear();
      this.attributeConfigurationDTOSubject.next(undefined);
      this.attributeConfigurationSubject.next(undefined);

      this.fetchAttributeConfiguration(idAttributeValueSet, idParentObject);
    }
    //Or load the AttributeValueSet if that changed (possible to switch from one event to another of the same type, for example
    //The AttributeConfiguration must also have finished loading and not already working on loading the values
    else if (this.idAttributeValueSet !== idAttributeValueSet && this.attributeConfigurationSubject.getValue()) {
      this.setAttributeValueSet(idAttributeValueSet, idParentObject);
    }
  }



  /**
   * Sets the attribute configuration identifiers to be used by this service.  This prompts a new configuration
   * request to be made.
   *
   * @param {number} idFilter1
   * @param {number} idFilter2
   */
/* This is commented out for now because it is both unimplemented and incomplete (loading a configuration
 * filter would really also require the attribute context and attributesecurity context plus a service on
 * the back end that actually does this work. For now, only loading by idAttributeConfiguration
 *
  setAttributeConfigurationByFilter(idFilter1: number, idFilter2?: number): void {
    if (isDevMode()) {
      console.debug("setAttributeConfigurationByFilter: " + idFilter1 + " " + idFilter2);
    }

    this.newAttributeConfiguration = undefined;
    //this.idAttributeConfiguration = undefined;
    this.idFilter1 = idFilter1;
    this.idFilter2 = idFilter2;

    this.fetchAttributeConfiguration();
  }
*/

  /**
   * Sets the id of the attribute value set.  This will prompt a request to pull this from the backend.
   *
   * @param {number} idAttributeValueSet
   */
  setAttributeValueSet(idAttributeValueSet: number, idParentObject: number) {
    //Only load the AttributeValueSet if it has been changed
    if (this.idAttributeValueSet !== idAttributeValueSet) {
      this.idAttributeValueSet = idAttributeValueSet;

      this.fetchAttributeValueSet(idParentObject);
    }
  }

  /**
   * The request to download the attribute configuration.
   */
  fetchAttributeConfiguration(idAttributeValueSet: number, idParentObject: number): void {

    if (this.newAttributeConfiguration) {
      this.attributeConfigurationDTOSubject.next(undefined);
      this.attributeConfigurationSubject.next(this.newAttributeConfiguration);
      return;
    } else if (this.idAttributeConfiguration === -1) {
      this.attributeConfigurationDTOSubject.next(undefined);
      this.attributeConfigurationSubject.next(<AttributeConfiguration>{
        idAttributeConfiguration: -2
      });
      return;
    }

    this.loadingSubject.next(true);

    let url: string;
    if (this.idAttributeConfiguration && this.idAttributeConfiguration > -1) {
      url = this.attributeEndpoint + "configurations/" + this.idAttributeConfiguration;
    } else {
      //This is not fully implemented
	  //url = this.attributeEndpoint + "configurations/filter/" + this.idFilter1;
      //if (this.idFilter2) {
        //url += "/" + this.idFilter2;
      //}
    }

    this.http.get<AttributeConfiguration>(url).subscribe((attributeConfiguration: AttributeConfiguration) => {
      this.setAttributeConfiguration(attributeConfiguration);
      this.loadingSubject.next(false);

      //Now load the attributeValueSet - This will always happen when a new configuration is loaded
      this.idAttributeValueSet = undefined; //Force it to change in this scenario
      this.setAttributeValueSet(idAttributeValueSet, idParentObject);
      this.dirty.next(false);
    },
    (error) => {
      console.error(error);
      this.loadingSubject.next(false);
      this.setAttributeConfiguration(undefined);
    });
  }

  setAttributeConfiguration(attributeConfiguration: AttributeConfiguration): void {
    this.attributeMap.clear();

    if (isDevMode()) {
      console.debug("AttributeService.setAttributeConfiguration");
      console.debug(attributeConfiguration);
    }

    if (attributeConfiguration) {
      if (attributeConfiguration.baseWindowTemplate) {
        this.getBaseWindowDimension(attributeConfiguration.baseWindowTemplate);
      }

      if (attributeConfiguration.attributeContainers) {
        attributeConfiguration.attributeContainers.sort((a: AttributeContainer, b: AttributeContainer) => {
          if (a.sortOrder < b.sortOrder) {
            return -1;
          } else if (a.sortOrder > b.sortOrder) {
            return 1;
          } else {
            return 0;
          }
        });
      }

      for (let attributeContainer of attributeConfiguration.attributeContainers) {
        /**
         * First sort attributes using the y and x positioning.  If using absolute position, the order doesn't matter,
         * but if we want the absolute positioning to guide auto layout, then use this order.
         */
        attributeContainer.graphicalAttributes.sort((a: GraphicalAttribute, b: GraphicalAttribute) => {
          if (a.tabOrder < b.tabOrder) {
            return -1;
          } else if (a.tabOrder > b.tabOrder) {
            return 1;
          } else {
            return 0;
          }
        });

        for (let attribute of attributeContainer.graphicalAttributes) {
          this.attributeMap.set(attribute.idAttribute, attribute);
        }
      }
    }

    this.attributeConfigurationDTOSubject.next(this.getAttributeConfigurationDTO(attributeConfiguration));
    this.attributeConfigurationSubject.next(attributeConfiguration);
  }

  /**
   * The request to pull the attribute value set.
   * Required the AttributeConfiguration to be pre-loaded
   */
  fetchAttributeValueSet(idParentObject: number): void {
    this.loadingSubject.next(true);
    this.idParentObject = idParentObject;

    let url: string = this.attributeEndpoint + "attribute-value-set/" + this.idAttributeValueSet;

    let ac: AttributeConfiguration = this.attributeConfigurationSubject.getValue();

    let queryParams: HttpParams = new HttpParams()
    .set("codeAttributeSecurityContext", ac.codeAttributeSecurityContext)
    .set("codeAttributeContext", ac.codeAttributeContext)
    .set("idParentObject", (idParentObject)?idParentObject.toString():"");

    //Clear ASAP
    this.slicedAttributeValues = [];
    this.updatedAttributeValues = [];
    this.attributeValueSetSubject.next(undefined);

    this.http.get<AttributeValueSet>(url, {params: queryParams}).subscribe((attributeValueSet: AttributeValueSet) => {
      //Initialize attributeValues if it does not exist
      if (! attributeValueSet.attributeValues) {
        attributeValueSet.attributeValues = [];
      }
      
      this.checkAvsEditable();

      this.attributeValueSetSubject.next(attributeValueSet);
      this.loadingSubject.next(false);
      this.dirty.next(false);
    },
    (error) => {
      console.error(error);
      this.loadingSubject.next(false);
      this.attributeValueSetSubject.next(undefined);
    });
  }
  
  checkAvsEditable(): void {
    let url: string = this.attributeEndpoint + "attribute-value-set/" + this.idAttributeValueSet + "/can-edit";

    let ac: AttributeConfiguration = this.attributeConfigurationSubject.getValue();

    let queryParams: HttpParams = new HttpParams()
    .set("codeAttributeSecurityContext", ac.codeAttributeSecurityContext)
    .set("codeAttributeContext", ac.codeAttributeContext)
    .set("idParentObject", (this.idParentObject)?this.idParentObject.toString():"");
  
    this.http.get<boolean>(url, {params: queryParams}).subscribe((canEdit: boolean) => {
      this.editButtonsEnabled = canEdit;
    });
  }

  getBaseWindowDimension(baseWindowTemplate: string): void {
    this.http.get(this.attributeEndpoint + "base-window-dimension", {params: new HttpParams().set("baseWindowTemplate", baseWindowTemplate)})
      .subscribe((dimension: any) => {
        this.attributeConfigurationDimensionSubject.next(dimension);
      });
  }

  /**
   * Currently a simple notifier that the configuration or attribute value set has changed and components may need to be refreshed.
   *
   */
  notifyAttributes(): void {
    this.containerUpdated.next(true);
  }

  /**
   * Post the attribute configuration after editing.  Post the entire thing as it cascades down.  The editor allows you
   * to modify every container and only then save the entire configuration.
   *
   * @param {AttributeConfiguration} attributeConfiguration
   */
  postAttributeConfiguration(attributeConfiguration: AttributeConfiguration): void {
    let url: string = this.attributeEndpoint + "configurations/" + attributeConfiguration.idAttributeConfiguration;

    this.loadingSubject.next(true);

    this.setNegativeIdsToUndefined(attributeConfiguration);

    this.http.post(url, attributeConfiguration).subscribe(() => {

      // Prevent error in subsequent saves. See Metabuilder / MB-23
      this.http.get<AttributeConfiguration>(url).subscribe((attributeConfiguration: AttributeConfiguration) => {
        if (isDevMode()) {
          console.debug(attributeConfiguration);
        }

        this.postConfigurationSubject.next(true);
        this.attributeConfigurationSubject.next(attributeConfiguration);
      },
      (error) => {
        console.error(error);
      },
      () => {
        this.loadingSubject.next(false);
      });

    },
    (error) => {
        console.error(error);
    });
  }

  /**
   * Put the attribute value set.  When editing, attributes already post their updated values to this services under
   * a separate array.  What we do is merge in these updated or created values with the current attribute value set (avs).
   * While doing this, set the id of the avs and nullify any negative ids.
   */
  updateAttributeValueSet(): Subject<AttributeValueSet> {
    let url: string = this.attributeEndpoint + "attribute-value-set/" ;

    let ac: AttributeConfiguration = this.attributeConfigurationSubject.getValue();

    let queryParams: HttpParams = new HttpParams()
    .set("codeAttributeSecurityContext", ac.codeAttributeSecurityContext)
    .set("codeAttributeContext", ac.codeAttributeContext)
    .set("idParentObject", (this.idParentObject)?this.idParentObject.toString():"");

    let avs: AttributeValueSet = Object.assign({}, this.attributeValueSetSubject.getValue());
    for (let i = this.updatedAttributeValues.length - 1; i >= 0; i--) {
      this.updatedAttributeValues[i].idAttributeValueSet = this.idAttributeValueSet;
      if (this.updatedAttributeValues[i].idAttributeValue < 0) {
        this.updatedAttributeValues[i].idAttributeValue = undefined;
      }

      let j: number = 0;
      for (j = 0; j < avs.attributeValues.length; j++) {
        if (this.updatedAttributeValues[i].idAttributeValue === avs.attributeValues[j].idAttributeValue) {
          break;
        }
      }
      if (j < avs.attributeValues.length) {
        avs.attributeValues[j] = this.updatedAttributeValues[i];
        this.updatedAttributeValues.splice(i, 1);
      }
    }
    for (let i = 0; i < this.slicedAttributeValues.length; i++) {
      let j: number = 0;
      for (j = 0; j < avs.attributeValues.length; j++) {
        if (this.slicedAttributeValues[i].idAttributeValue === avs.attributeValues[j].idAttributeValue) {
          break;
        }
      }
      if (j < avs.attributeValues.length) {
        avs.attributeValues.splice(j, 1);
      }
    }
    avs.attributeValues = avs.attributeValues.concat(this.updatedAttributeValues);

    this.loadingSubject.next(true);

    let resultSubject: Subject<AttributeValueSet> = new Subject<AttributeValueSet>();

    this.http.put(url, avs, {params: queryParams}).subscribe((attributeValueSet: AttributeValueSet) => {
        if (isDevMode()) {
          console.debug(attributeValueSet);
        }

        //Initialize attributeValues if it does not exist
        if (! attributeValueSet.attributeValues) {
          attributeValueSet.attributeValues = [];
        }

        this.slicedAttributeValues = [];
        this.updatedAttributeValues = [];
        this.attributeValueSetSubject.next(attributeValueSet);
        this.loadingSubject.next(false);
        this.dirty.next(false);

        //Send the results to the observer (if any), then complete
        resultSubject.next(attributeValueSet);
        resultSubject.complete();
      },
      (error) => {
        console.error(error);
        this.loadingSubject.next(false);
        resultSubject.error(error);
      });

    return resultSubject;
  }

  /**
   * Get the attributeConfigurationSubject.
   *
   * @returns {BehaviorSubject<AttributeConfiguration>}
   */
  getAttributeConfigurationSubject(): BehaviorSubject<AttributeConfiguration> {
    return this.attributeConfigurationSubject;
  }

  getDirtySubject(): BehaviorSubject<boolean> {
    return this.dirty;
  }

  /**
   * Get the attributeValueSetSubject;
   *
   * @returns {BehaviorSubject<AttributeValueSet>}
   */
  getAttributeValueSet(): BehaviorSubject<AttributeValueSet> {
    return this.attributeValueSetSubject;
  }

  /**
   * Get the attributeValuePushedSubject;
   *
   * @returns {Subject<AttributeValue>}
   */
  getAttributeValuePushedSubject(): Subject<AttributeValue> {
    return this.attributeValuePushedSubject;
  }

  /**
   * Get all attribute values based upon the idAttribute.
   *
   * @param {number} idAttributes
   * @returns {AttributeValue[]}
   */
  getAttributeValues(idAttribute: number, groupAttributeRowId: number): AttributeValue[] {

    let attributeValues: AttributeValue[] = [];

    if(this.attributeValueSetSubject.getValue()) {

      //Go through all the updated attribute values first (so we preserve across changing tabs, etc)
      let i: number = 0;
      for (i = 0; i < this.updatedAttributeValues.length; i++) {
        if (this.updatedAttributeValues[i].idAttribute === idAttribute && (!groupAttributeRowId || groupAttributeRowId === this.updatedAttributeValues[i].groupAttributeRowId)) {
          attributeValues.push(Object.assign({}, this.updatedAttributeValues[i]));
        }
        else if (this.updatedAttributeValues[i].idGroupAttribute === idAttribute && (!groupAttributeRowId || groupAttributeRowId === this.updatedAttributeValues[i].groupAttributeRowId)) {
          attributeValues.push(Object.assign({}, this.updatedAttributeValues[i]));
        }
      }

      //Go through all the regular attributes
      for (let attributeValue of this.attributeValueSetSubject.getValue().attributeValues) {
        //Regular attributes (normal and group, which get multiple)
        if (attributeValue.idAttribute === idAttribute || attributeValue.idGroupAttribute === idAttribute) {
          //If row id specified, only care about values for that row
          if (typeof groupAttributeRowId === 'undefined' || attributeValue.groupAttributeRowId === groupAttributeRowId) {

            //If not already added...
            if (! attributeValues.find((existing) => existing.idAttributeValue === attributeValue.idAttributeValue)) {

              //Don't want the original if it was removed (sliced)
              if (! this.slicedAttributeValues.find((sav) => sav.idAttributeValue === attributeValue.idAttributeValue)) {
                attributeValues.push(Object.assign({}, attributeValue));
              }
            }
          }
        }
      }
    }

    return attributeValues;
  }

  /**
   * Get all child attributes of the idAttribute.  Most likely for attributes that define columns of a grid.
   *
   * @param {number} idAttribute
   * @returns {Attribute[]}
   */
  getGroupAttributes(idAttribute: number): Attribute[] {
    let attributes: Attribute[] = [];

    this.attributeMap.forEach((attribute: Attribute) => {
      if (attribute.idGroupAttribute === idAttribute) {
        attributes.push(attribute);
      }
    });

    return attributes;
  }

  /**
   * Set the width which is a requirement of the absolute positioning.
   *
   * @param {number} width
   */
  setWidth(width: number): void {
    this.width = width;
  }

  /**
   * Get the width.
   *
   * @returns {number}
   */
  getWidth(): number {
    return this.width;
  }

  /**
   * Get the array of attribute choices for the idAttribute.
   *
   * @param {number} idAttribute
   * @returns {AttributeChoice[]}
   */
  getAttributeChoices(idAttribute: number): AttributeChoice[] {
    return this.attributeMap.get(idAttribute).attributeChoices;
  }

  getLoadingSubject(): Subject<boolean> {
    return this.loadingSubject;
  }

  /**
   * Get the containerUpdated subject.
   *
   * @returns {Subject<number>}
   */
  getContainerUpdated(): Subject<boolean> {
    return this.containerUpdated;
  }

  getGridRowSavedSubject(): Subject<AttributeValueGridRow> {
    return this.gridRowSaved;
  }

  getGridRowDeletedSubject(): Subject<AttributeValueGridRow> {
    return this.gridRowDeleted;
  }

  /**
   * Clear the updated and deleted attribute value arrays.
   *
   */
  clearUpdatedAttributeValues(): void {
    this.slicedAttributeValues = [];
    this.updatedAttributeValues = [];
    this.dirty.next(false);
    this.notifyAttributes();
  }

  /**
   * Clear the updated and deleted grid row attributes for the specified grid row
   *
   */
  clearUpdatedGridRowAttributeValues(groupAttributeRowId: number): void {
    let remainingAttributes = [];

    this.slicedAttributeValues = this.slicedAttributeValues.filter((av) => av.groupAttributeRowId !== groupAttributeRowId);
    this.updatedAttributeValues =  this.updatedAttributeValues.filter((av) => av.groupAttributeRowId !== groupAttributeRowId);

    //Shouldn't be a need to change dirty or notify attributes
  }

  saveGridRowAttributeValues(idGroupAttribute: number, groupAttributeRowId: number): void {
    let rowValues = this.updatedAttributeValues.filter((av) => av.groupAttributeRowId === groupAttributeRowId && av.idGroupAttribute === idGroupAttribute);

    for (let j = 0; j < rowValues.length; j++) {
      //set the idAttributeValueSet if necessary
      if (! rowValues[j].idAttributeValueSet) {
        rowValues[j].idAttributeValueSet = this.idAttributeValueSet;
      }

      //remove all negative ids (new values)
      if (rowValues[j].idAttributeValue < 0) {
        rowValues[j].idAttributeValue = undefined;
      }
    }

    //For multi values that were sliced (de-selected) we're going to find them and null out the value, instead of removing.
    //That means they actaully need to be added to the rowValues
    //Removed multi values within an existing row
    let removedValues = this.slicedAttributeValues.filter((av) => av.groupAttributeRowId === groupAttributeRowId && av.idGroupAttribute === idGroupAttribute);
    for (let i = 0; i < removedValues.length; i++) {
      removedValues[i].valueAttributeChoice = undefined;
      removedValues[i].valueIdDictionary = undefined;
      rowValues.push(removedValues[i]);
    }

    let avgr:AttributeValueGridRow = <AttributeValueGridRow>{
        idAttributeValueSet: this.idAttributeValueSet,
        idGroupAttribute: idGroupAttribute,
        groupAttributeRowId: groupAttributeRowId,
        attributeValues: rowValues
      };

    let url: string = this.attributeEndpoint + "attribute-value-grid-row/" ;

    let ac: AttributeConfiguration = this.attributeConfigurationSubject.getValue();

    let queryParams: HttpParams = new HttpParams()
    .set("codeAttributeSecurityContext", ac.codeAttributeSecurityContext)
    .set("codeAttributeContext", ac.codeAttributeContext)
    .set("idParentObject", (this.idParentObject)?this.idParentObject.toString():"");


    this.loadingSubject.next(true);

    //Save new row
    if (groupAttributeRowId < 0) {
      this.http.post(url, avgr, {params: queryParams}).subscribe((attributeValueGridRow: AttributeValueGridRow) => {

        //Add the grid row to the original attribute values
        let avs:AttributeValueSet = this.attributeValueSetSubject.getValue();

        for (let i = 0; i < attributeValueGridRow.attributeValues.length; i++) {
          avs.attributeValues.push(attributeValueGridRow.attributeValues[i]);
        }

        //Remove the updated and sliced values for the grid row, now (these are the old ones with the negative rowid)
        this.slicedAttributeValues = this.slicedAttributeValues.filter((av) => (av.groupAttributeRowId !== groupAttributeRowId || av.idGroupAttribute !== idGroupAttribute));
        this.updatedAttributeValues =  this.updatedAttributeValues.filter((av) => (av.groupAttributeRowId !== groupAttributeRowId || av.idGroupAttribute !== idGroupAttribute));

        this.attributeValueSetSubject.next(avs);
        this.gridRowSaved.next(attributeValueGridRow);
        this.loadingSubject.next(false);
      },
      (error) => {
        console.error(error);
        this.loadingSubject.next(false);
      });
    }
    //Update existing row
    else {
      this.http.put(url, avgr, {params: queryParams}).subscribe((attributeValueGridRow: AttributeValueGridRow) => {

        //Add the grid row to the original attribute values
        let avs:AttributeValueSet = this.attributeValueSetSubject.getValue();

        //Remove all old values for row
        let index = avs.attributeValues.findIndex((av)=> (av.groupAttributeRowId === groupAttributeRowId && av.idGroupAttribute === idGroupAttribute));
        while (index >= 0) {
          //Remove value
          avs.attributeValues.splice(index, 1);

          //Find the next one
          index = avs.attributeValues.findIndex((av)=> (av.groupAttributeRowId === groupAttributeRowId && av.idGroupAttribute === idGroupAttribute));
        }

        //Add all the current values
        for (let i = 0; i < attributeValueGridRow.attributeValues.length; i++) {
          avs.attributeValues.push(attributeValueGridRow.attributeValues[i]);
        }

        //Remove the sliced and updated values for the grid row
        this.slicedAttributeValues = this.slicedAttributeValues.filter((av) => (av.groupAttributeRowId !== groupAttributeRowId || av.idGroupAttribute !== idGroupAttribute));
        this.updatedAttributeValues =  this.updatedAttributeValues.filter((av) => (av.groupAttributeRowId !== groupAttributeRowId || av.idGroupAttribute !== idGroupAttribute));

        this.attributeValueSetSubject.next(avs);
        this.gridRowSaved.next(attributeValueGridRow);
        this.loadingSubject.next(false);
      },
      (error) => {
        console.error(error);
        this.loadingSubject.next(false);
      });
    }
  }

  deleteGridRowAttributeValues(idGroupAttribute: number, groupAttributeRowId: number): void {
    let url: string = this.attributeEndpoint + "attribute-value-grid-row/" ;

    let ac: AttributeConfiguration = this.attributeConfigurationSubject.getValue();

    let queryParams: HttpParams = new HttpParams()
    .set("codeAttributeSecurityContext", ac.codeAttributeSecurityContext)
    .set("codeAttributeContext", ac.codeAttributeContext)
    .set("idParentObject", (this.idParentObject)?this.idParentObject.toString():"");

    let avgr:AttributeValueGridRow = <AttributeValueGridRow>{
        idAttributeValueSet: this.idAttributeValueSet,
        idGroupAttribute: idGroupAttribute,
        groupAttributeRowId: groupAttributeRowId,
        attributeValues: []
      };

    this.loadingSubject.next(true);

    this.http.request('delete', url, {body: avgr, params: queryParams}).subscribe(() => {

      //Remove the grid row from the original attribute values
      let avs:AttributeValueSet = this.attributeValueSetSubject.getValue();
      //Filter to only attributes to KEEP - (i.e. not the same grid or not the row being deleted)
      avs.attributeValues = avs.attributeValues.filter((av) => (av.idGroupAttribute !== idGroupAttribute || av.groupAttributeRowId !== groupAttributeRowId));
      this.attributeValueSetSubject.next(avs);
      this.gridRowDeleted.next(avgr);
      this.loadingSubject.next(false);
    },
    (error) => {
      console.error(error);
      this.loadingSubject.next(false);
    });
  }

  /**
   * Used to refresh a grid row after plugin execution without affecting other attribute values
   * Normally saving the grid row updates the values, but some applications execute a subsequent plugin and need an extra refresh to pick up changes.
   *
   * @param {AttributeValueGridRow} avgr
   */
  refreshGridRow(avgr: AttributeValueGridRow) {
    let url: string = this.attributeEndpoint + "attribute-value-grid-row/" ;

    let ac: AttributeConfiguration = this.attributeConfigurationSubject.getValue();

    let queryParams: HttpParams = new HttpParams()
    .set("idGroupAttribute", avgr.idGroupAttribute)
    .set("groupAttributeRowId", avgr.groupAttributeRowId)
    .set("idAttributeValueSet", avgr.idAttributeValueSet)
    .set("codeAttributeSecurityContext", ac.codeAttributeSecurityContext)
    .set("codeAttributeContext", ac.codeAttributeContext)
    .set("idParentObject", (this.idParentObject)?this.idParentObject.toString():"");

    this.loadingSubject.next(true);

    this.http.get<AttributeValueGridRow>(url, {params: queryParams}).subscribe((attributeValueGridRow: AttributeValueGridRow) => {

      //Note that the sliced and updated values for the grid row were already cleaned up during the save

      //Remove the grid row from the original attribute values
      let avs:AttributeValueSet = this.attributeValueSetSubject.getValue();

      //Remove all old values for row
      let index = avs.attributeValues.findIndex((av)=> (av.groupAttributeRowId === attributeValueGridRow.groupAttributeRowId && av.idGroupAttribute === attributeValueGridRow.idGroupAttribute));
      while (index >= 0) {
        //Remove value
        avs.attributeValues.splice(index, 1);

        //Find the next one
        index = avs.attributeValues.findIndex((av)=> (av.groupAttributeRowId === attributeValueGridRow.groupAttributeRowId && av.idGroupAttribute === attributeValueGridRow.idGroupAttribute));
      }

      //Add all the current values
      for (let i = 0; i < attributeValueGridRow.attributeValues.length; i++) {
        avs.attributeValues.push(attributeValueGridRow.attributeValues[i]);
      }


      this.attributeValueSetSubject.next(avs);
      this.loadingSubject.next(false);
    },
    (error) => {
      console.error(error);
      this.loadingSubject.next(false);
    });

  }

  /**
   * Push an attribute to the attribute map.
   *
   * @param {Attribute} attribute
   */
  pushAttribute(attribute: Attribute): void {
    this.attributeMap.set(attribute.idAttribute, attribute);
  }


  pushAttributeValueMultiChoice(attributeValue: AttributeValue): void {
    //Remove from sliced if present
    let i: number = 0;
    for (i = 0; i < this.slicedAttributeValues.length; i++) {
      if (this.slicedAttributeValues[i].idAttribute === attributeValue.idAttribute && this.slicedAttributeValues[i].valueAttributeChoice.idAttributeChoice === attributeValue.valueAttributeChoice.idAttributeChoice) {
        break;
      }
    }
    //Remove from sliced attribute values (no longer removing)
    if (i < this.slicedAttributeValues.length) {
      this.slicedAttributeValues.splice(i, 1);
    }
    //Otherwise push the value, so it will be saved
    else {
      this.pushAttributeValue(attributeValue);
    }
  }

  pushAttributeValueMultiDict(attributeValue: AttributeValue): void {
    //Remove from sliced if present
    let i: number = 0;
    for (i = 0; i < this.slicedAttributeValues.length; i++) {
      if (this.slicedAttributeValues[i].idAttribute === attributeValue.idAttribute && this.slicedAttributeValues[i].valueIdDictionary === attributeValue.valueIdDictionary) {
        break;
      }
    }
    //Remove from sliced attribute values (no longer removing)
    if (i < this.slicedAttributeValues.length) {
      this.slicedAttributeValues.splice(i, 1);
    }
    //Otherwise push the value, so it will be saved
    else {
      this.pushAttributeValue(attributeValue);
    }
  }

  /**
   * Push a new or updated attribute value.  The current one is just removed and the new one appended to the array.
   *
   * @param {AttributeValue} attributeValue
   */
  pushAttributeValue(attributeValue: AttributeValue): void {
    if (isDevMode()) {
      console.debug("pushAttributeValue");
      console.debug(attributeValue);
    }

    let i: number = 0;
    for (i = 0; i < this.updatedAttributeValues.length; i++) {
      if (this.updatedAttributeValues[i].idAttributeValue === attributeValue.idAttributeValue) {
        break;
      }
    }
    if (i < this.updatedAttributeValues.length) {
      this.updatedAttributeValues.splice(i, 1);
    }

    this.updatedAttributeValues.push(attributeValue);
    this.attributeValuePushedSubject.next(attributeValue);

    //Only setting dirty for non grid-rows (those are saved immediately)
    if (! attributeValue.idGroupAttribute) {
      this.dirty.next(true);
    }
  }

  /**
   * For pushing multiple attribute values at once.  This would typically be for a multi choice.
   *
   * @param {Attribute} attribute
   * @param {AttributeValue[]} attributeValues
   */
  pushAttributeValues(attribute: Attribute, attributeValues: AttributeValue[]): void {
    if (isDevMode()) {
      console.debug("pushAttributeValues");
      console.debug(attributeValues);
    }

    for (let i = 0; i < attributeValues.length; i++) {
      let j: number = 0;
      for (j = 0; j < this.updatedAttributeValues.length; j++) {
        if (this.updatedAttributeValues[j].idAttribute !== attribute.idAttribute) {
          continue;
        } else if (this.updatedAttributeValues[j].idAttributeValue === attributeValues[i].idAttributeValue) {
          break;
        }
      }
      if (j < this.updatedAttributeValues.length) {
        this.updatedAttributeValues[j] = attributeValues[i];
      } else {
        this.updatedAttributeValues.push(attributeValues[i]);
      }
    }

    if (isDevMode()) {
      console.debug(this.updatedAttributeValues);
    }

    //Only setting dirty for non grid-rows (those are saved immediately)
    if (! attribute.idGroupAttribute) {
      this.dirty.next(true);
    }
  }

  /**
   * Push a value in the case where an attribute value doesn't exist.
   *
   * @param {Attribute} attribute
   * @param value
   */
  pushValue(attribute: Attribute, value: any): void {
    let attributeValue: AttributeValue = <AttributeValue>{
      idAttribute: attribute.idAttribute,
      idAttributeValueSet: this.idAttributeValueSet
    };

    if (attribute.codeAttributeDataType === "TXT") {
      attributeValue.valueLongText = {
        idLongText: undefined,
        textData: value
      };
    }

    this.updatedAttributeValues.push(attributeValue);
    this.dirty.next(true);
  }

  /**
   * Remove an attribute value.  Typically in the case of a multi select where the unselected value is removed.
   *
   * @param {Attribute} attribute
   * @param {AttributeChoice} attributeChoice
   */
  spliceAttributeValueChoice(attribute: Attribute, attributeChoice: AttributeChoice): void {
    if (isDevMode()) {
      console.debug("sliceAttributeValue: " + attribute.idAttribute + ", " + attributeChoice.idAttributeChoice);
    }

    let i: number = 0;
    for (i = 0; i < this.updatedAttributeValues.length; i++) {
      if (this.updatedAttributeValues[i].idAttribute === attribute.idAttribute && this.updatedAttributeValues[i].valueAttributeChoice.idAttributeChoice === attributeChoice.idAttributeChoice) {
        break;
      }
    }
    //Remove from updated attribute values (not previously saved)
    if (i < this.updatedAttributeValues.length) {
      this.updatedAttributeValues.splice(i, 1);
    }
    //Add to sliced values (previously saved - need to remove)
    else {
      for (i = 0; i < this.attributeValueSetSubject.getValue().attributeValues.length; i++) {
        if (this.attributeValueSetSubject.getValue().attributeValues[i].idAttribute === attribute.idAttribute
            && this.attributeValueSetSubject.getValue().attributeValues[i].valueAttributeChoice.idAttributeChoice === attributeChoice.idAttributeChoice) {
          this.slicedAttributeValues.push(this.attributeValueSetSubject.getValue().attributeValues[i]);
          break;
        }
      }
    }

    //Only flagging dirty for non-grid rows (those are saved immediately)
    if (! attribute.idGroupAttribute) {
      this.dirty.next(true);
    }
  }


  /**
   * Remove an attribute value.  Typically in the case of a multi select where the unselected value is removed.
   *
   * @param {Attribute} attribute
   * @param {AttributeChoice} attributeChoice
   */
  spliceAttributeValueDict(attribute: Attribute, value: any): void {
    if (isDevMode()) {
      console.debug("sliceAttributeValue: " + attribute.idAttribute + ", " + value);
    }

    let i: number = 0;
    for (i = 0; i < this.updatedAttributeValues.length; i++) {
      if (this.updatedAttributeValues[i].idAttribute === attribute.idAttribute && this.updatedAttributeValues[i].valueIdDictionary === value) {
        break;
      }
    }
    //Remove from updated attribute values (not previously saved)
    if (i < this.updatedAttributeValues.length) {
      this.updatedAttributeValues.splice(i, 1);
    }
    //Add to sliced values (previously saved - need to remove)
    else {
      for (i = 0; i < this.attributeValueSetSubject.getValue().attributeValues.length; i++) {
        if (this.attributeValueSetSubject.getValue().attributeValues[i].idAttribute === attribute.idAttribute
            && this.attributeValueSetSubject.getValue().attributeValues[i].valueIdDictionary === value) {
          this.slicedAttributeValues.push(this.attributeValueSetSubject.getValue().attributeValues[i]);
          break;
        }
      }
    }

    //Only flagging dirty for non-grid rows (those are saved immediately)
    if (! attribute.idGroupAttribute) {
      this.dirty.next(true);
    }
  }

  /**
   * Get the attribute based upon the idAttribute.  This is from the map of attributes.
   *
   * @param {number} idAttribute
   * @returns {Attribute}
   */
  getAttribute(idAttribute: number): Attribute {
    return this.attributeMap.get(idAttribute);
  }

  /**
   * For the attribute configuration, set all of the negative ids to undefined prior to posting.
   *
   * @param {AttributeConfiguration} attributeConfiguration
   */
  setNegativeIdsToUndefined(attributeConfiguration: AttributeConfiguration): void {
    if (attributeConfiguration.idAttributeConfiguration < 0) {
      attributeConfiguration.idAttributeConfiguration = undefined;
    }

    for (let attributeContainer of attributeConfiguration.attributeContainers) {
      if (attributeContainer.idAttributeContainer  < 0) {
        attributeContainer.idAttributeContainer = undefined;
      }

      for (let attribute of attributeContainer.graphicalAttributes) {

        if (attribute.idAttribute  < 0) {
          attribute.idAttribute = undefined;
        }

        if (attribute.attributeChoices) {
          for (let attributeChoice of attribute.attributeChoices) {
            delete attribute["value"];

            if (attributeChoice.idAttribute  < 0) {
              attributeChoice.idAttribute = undefined;
            }
            if (attributeChoice.idAttributeChoice  < 0) {
              attributeChoice.idAttributeChoice = undefined;
            }
          }
        }
      }
    }
  }

  /**
   * Creates a display name for the attribute configuration based on the context code and id.
   *
   * @param {AttributeConfiguration} configuration
   * @returns {string}
   */
  getConfigurationName(configuration: AttributeConfigurationDTO): string {
    if (configuration.idAttributeConfiguration === -1) {
      return "New";
    }

    let name: string = configuration.codeAttributeSecurityContextDisplay + ": " + configuration.extensionDescription;

    if (configuration.idFilter1) {
      name += " - " + configuration.filter1Display;
    }
    if (configuration.idFilter2) {
      name += " - " + configuration.filter2Display;
    }
    return name;
  }

  getAttributeConfigurationDTO(cfg: AttributeConfiguration): AttributeConfigurationDTO {
    if (!cfg) {
      return undefined;
    }

    let dto: AttributeConfigurationDTO = <AttributeConfigurationDTO>{
      idAttributeConfiguration: cfg.idAttributeConfiguration,
      codeAttributeContext: cfg.codeAttributeContext,
      codeAttributeSecurityContext: cfg.codeAttributeSecurityContext,
      idFilter1: cfg.idFilter1,
      idFilter2: cfg.idFilter2
    };

    if (this.attributeContextListSubject.getValue()) {
      dto.extensionDescription = this.attributeContextListSubject.getValue().filter((ctx: any) => {
        return ctx.codeAttributeContext === cfg.codeAttributeContext;
      })[0].extensionDescription;
    }

    if (this.attributeSecurityContextListSubject.getValue()) {
      let result: any[] = this.attributeSecurityContextListSubject.getValue().filter((filter: any) => {
        return filter.codeAttributeSecurityContext === cfg.codeAttributeSecurityContext;
      });
      dto.codeAttributeSecurityContextDisplay = (result && result.length > 0) ? result[0].description : undefined;
    }

    if (this.filter1ListSubject.getValue()) {
      let result: any[] = this.filter1ListSubject.getValue().filter((filter: any) => {
        return filter.codeAttributeContext === cfg.codeAttributeContext
          && filter.codeAttributeSecurityContext === cfg.codeAttributeSecurityContext
          && filter.idFilter1 === cfg.idFilter1;
      });
      dto.filter1Display = (result && result.length > 0) ? result[0].display : undefined;
    }

    if (this.filter2ListSubject.getValue()) {
      let result: any[] = this.filter2ListSubject.getValue().filter((filter: any) => {
        return filter.codeAttributeContext === cfg.codeAttributeContext
          && filter.codeAttributeSecurityContext === cfg.codeAttributeSecurityContext
          && filter.idFilter1 === cfg.idFilter1
          && filter.idFilter2 === cfg.idFilter2;
      });
      dto.filter2Display = (result && result.length > 0) ? result[0].display : undefined;
    }

    return dto;
  }

  getSecurityCodeContext(code: string): any {
    return this.attributeSecurityContextListSubject.getValue().filter((filter: any) => {
      return filter.codeAttributeSecurityContext === code;
    })[0];
  }

  getAttributeContextLoadingSubject(): BehaviorSubject<boolean> {
    return this.attributeContextLoadingSubject;
  }

  getFilter1For(codeAttributeContext: string, codeAttributeSecurityContext: string): any[] {
    return this.filter1ListSubject.getValue().filter((item: any) => {
      return item.codeAttributeContext === codeAttributeContext && item.codeAttributeSecurityContext === codeAttributeSecurityContext;
    });
  }

  getFilter2For(codeAttributeContext: string, codeAttributeSecurityContext: string, idFilter1: number): any[] {
    return this.filter2ListSubject.getValue().filter((item: any) => {
      return item.codeAttributeContext === codeAttributeContext
        && item.codeAttributeSecurityContext === codeAttributeSecurityContext
        && item.idFilter1 === idFilter1;
    });
  }

  /**
   * Track new idAttributeValues via a negative number for internal tracking.  Remove this fake id prior to post.
   *
   * @returns {number}
   */
  getUniqueId(): number {
    return --this.uniqueId;
  }
}
