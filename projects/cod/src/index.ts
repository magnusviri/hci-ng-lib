
/*
 * Copyright (c) 2016 Huntsman Cancer Institute at the University of Utah, Confidential and Proprietary
 */
/**
 * A barrel file for the cod package.
 *
 * @since 1.0.0
 */

import {AttributeAbsoluteComponent} from "./components/attribute-absolute.component";

export {CodModule} from "./cod.module";

export {AttributeConfigurationComponent} from "./components/attribute-configuration.component";

export {AttributeService} from "./services/attribute.service";

export {Attribute} from "./model/attribute.entity";
export {AttributeChoice} from "./model/attribute-choice.entity";
export {AttributeConfiguration} from "./model/attribute-configuration.entity";
export {AttributeContainer} from "./model/attribute-container.entity";
export {AttributeDictionary} from "./model/attribute-dictionary.entity";
export {AttributeValue} from "./model/attribute-value.entity";
export {AttributeValueGridRow} from "./model/attribute-value-grid-row.entity";
export {AttributeValueSet} from "./model/attribute-value-set.entity";
export {ExtractableFieldStatus} from "./model/extractable-field-status.entity";
export {AttributeConfigurationDTO} from "./model/attribute-configuration.dto";
export {GraphicalAttribute} from "./model/graphical-attribute.entity";

export {AttributeBase} from "./components/attribute-base";
export {AttributeAbsoluteComponent} from "./components/attribute-absolute.component";
export {AttributeFlexComponent} from "./components/attribute-flex.component";
export {AttributeEditComponent} from "./components/attribute-edit.component";
export {AttributeDefaultComponent} from "./components/attribute-default.component";
export {AttributeContainerComponent} from "./components/attribute-container.component";

export {IsGroupAttributePipe} from "./pipes/is-group-attribute.pipe";

export {ATTRIBUTE_ENDPOINT} from "./services/attribute.service";
