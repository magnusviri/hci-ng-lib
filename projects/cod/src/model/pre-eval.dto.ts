import {Attribute} from "./attribute.entity";

export class PreEvalDTO {
  expression: string;
  referencedAttribute: Attribute;
}