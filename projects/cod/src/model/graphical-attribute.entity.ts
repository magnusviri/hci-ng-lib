import {Attribute} from "./attribute.entity";

export interface GraphicalAttribute extends Attribute {
  idAttributeContainer: number;
  x: number;
  y: number;
  h: number;
  w: number;
  tabOrder: number;
  altioFormat: string;
  dropFilterControlName: string;
  dropFilterControlProperty: string;
  dropFilterAttribute: string;
}
