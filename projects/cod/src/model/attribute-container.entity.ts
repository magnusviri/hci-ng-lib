import {GraphicalAttribute} from "./graphical-attribute.entity";

export interface AttributeContainer {
  idAttributeContainer: number;
  idAttributeConfiguration: number;
  containerName: string;
  isAutoLayout?: string;
  sortOrder?: number;
  graphicalAttributes: GraphicalAttribute[];
}
