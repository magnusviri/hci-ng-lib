import {AttributeChoice} from "./attribute-choice.entity";
import {AttributeDictionary} from "./attribute-dictionary.entity";

export interface Attribute {
  idAttribute: number;
  idGroupAttribute: number;
  subclass: string;
  attributeName: string;
  displayName: string;
  reportingName: string;
  codeAttributeDataType: string;
  isMultiValue: string;
  isAjcc: string;
  isCap: string;
  isActive: string;
  isCalculated: string;
  isRequired: string;
  defaultValue: string;
  idAjccParameterName: number;
  idAttributeDictionary: number;
  attributeDictionary: AttributeDictionary;
  groupAttribute: Attribute;
  attributes: Attribute[];
  attributeChoices: AttributeChoice[];
  idAttributeConfiguration: number;
  attributeDescription: string;
}
