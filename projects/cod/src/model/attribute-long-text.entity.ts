export interface AttributeLongText {
  idLongText: number;
  textData: string;
}
