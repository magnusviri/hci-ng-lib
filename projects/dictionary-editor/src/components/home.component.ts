import {Component, HostBinding} from "@angular/core";

/**
 * Splash screen for the editor when no dictionary is selected.
 *
 * TODO: Use this page to put more of a guide on how to use the editor?
 *
 * @since 1.0.0
 */
@Component({
  selector: "core-dictionary-editor-home",
  template: `
    <div class="d-flex m-3 flex-column flex-grow">
      <h4>Dictionary Editor</h4>

      <div class="d-flex">
        Select a dictionary from the sidebar to view its properties and data.
      </div>
    </div>
  `
})
export class DictionaryEditorHomeComponent {

  @HostBinding("class") classList: string = "outlet-column y-auto";

}
