import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";

import { AgGridModule } from "ag-grid-angular";

import {MatDatepickerModule} from "@angular/material/datepicker";

import { SelectEditor } from "./select.editor";
import {AgGridRendererModule} from "../grid-renderers/ag-grid-renderer.module";
import {DatepickerComponent} from "./datepicker.component";
import {AgGridDateCellEditorComponent} from "./ag-grid-date-cell-editor.component";
import {MomentDateModule} from "@angular/material-moment-adapter";


@NgModule({
    imports: [
        AgGridModule,
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        MatDatepickerModule,
        MomentDateModule,
        AgGridRendererModule
    ],
    declarations: [
        SelectEditor,
        AgGridDateCellEditorComponent,
        DatepickerComponent
    ],
    exports: [
        SelectEditor,
        AgGridDateCellEditorComponent
    ]
})
export class AgGridEditorModule { }
