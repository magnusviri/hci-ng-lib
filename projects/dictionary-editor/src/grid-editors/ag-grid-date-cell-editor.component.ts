import {Component} from "@angular/core";
import {ICellEditorAngularComp} from "ag-grid-angular";

@Component({
    selector: "ag-grid-date-cell-editor",
    template: `
        <hci-dict-datepicker [(ngModel)]="value" placeholder="Select Date"></hci-dict-datepicker>

    `
})
export class AgGridDateCellEditorComponent implements ICellEditorAngularComp {
    public value: any;
    params: any;

    agInit(params: any): void {
        console.log("agInit Date Picker");
        console.log(params);
        this.params = params;
        this.value = this.params.value;
    }

    getValue() {
        return this.value;
    }

    isPopup(): boolean {
      return true;
    };
}
