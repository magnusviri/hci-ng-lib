import {Component, EventEmitter, forwardRef, Input, Output} from "@angular/core";
import {MAT_DATE_LOCALE, MAT_DATE_FORMATS} from "@angular/material/core";
import {ControlValueAccessor, NG_VALUE_ACCESSOR} from "@angular/forms";

@Component({
  selector: "hci-dict-datepicker",
  template: `
    <div class="d-flex" style="width: 100%;">
      <input matInput [matDatepicker]="picker" class="form-control" [placeholder]="placeholder" [(ngModel)]="value" (ngModelChange)="onChange($event)"
             (blur)="onBlur($event)" data-date-format="YYYY-MM-DD"
             [ngStyle]="{'background-color':invalid ? 'red' : '#fff'}"   >
      <span class="input-group-text" id="clearIcon" (click)="clear()"><i class="fas fa-times"></i></span>
      <button class="btn btn-outline-secondary" type="button" (click)="picker.open()" style="height:32px; width:32px;"><i class="fas fa-calendar-alt"></i></button>
      <mat-datepicker panelClass="ag-custom-component-popup" #picker ></mat-datepicker>
    </div>
  `,
  styles: [`
    #clearIcon {
      cursor: pointer;
    }
    #clearIcon:Hover {
      background-color: rgb(73, 80, 87);
      color: rgb(233, 236, 239)
    }
  `],
  providers: [
    {provide: MAT_DATE_LOCALE, useValue: "en-GB"},
    {
      provide: MAT_DATE_FORMATS,
      useValue: {
        parse: {
          dateInput: ["YYYY-MM-DD"],
        },
        display: {
          dateInput: "YYYY-MM-DD",
          monthYearLabel: "MMM YYYY",
          dateA11yLabel: "YYYY-MM-DD",
          monthYearA11yLabel: "MMMM YYYY",
        },
      },
    },
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => DatepickerComponent),
      multi: true
    }
  ]
})
export class DatepickerComponent implements ControlValueAccessor {
  //{provide: MAT_DATE_LOCALE, useValue: "en-GB"},
  // @Input() value: Date;
  value: Date;
  @Input() placeholder: string;
  @Input() invalid: boolean = false;

  // @Output() valueChange = new EventEmitter();

  onChange: any;

  ngOnInit() {

  }

  // onChange(newValue) {
  //   this.value = newValue;
  //   this.valueChange.emit(newValue);
  // }

  clear() {
    this.value = undefined;
    // this.valueChange.emit();
  }

  // Clear input field if entered data is invalid (Mat library sets value to null if invalid but doesn't clear input)
  onBlur(newValue) {

    if (!this.value) {
      this.value = undefined;
      // this.valueChange.emit();
    }
  }

  writeValue(value: any) {
    if (value !== undefined) {
      this.value = value;
    }
  }

  registerOnChange(fn) {
    this.onChange = () => {
      fn(this.value);
    };
  }

  registerOnTouched(fn: () => void): void {}

}

