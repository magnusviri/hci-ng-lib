
export {DictionaryEditorModule} from "./dictionary-editor.module";

export {DictionaryEditorDetailComponent} from "./components/detail.component";

export {DictionaryEditorHomeComponent} from "./components/home.component";
export {DictionaryEditorComponent} from "./dictionary-editor.component";
