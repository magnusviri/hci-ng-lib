/*
 * Copyright (c) 2016 Huntsman Cancer Institute at the University of Utah, Confidential and Proprietary
 */
import {Component, ViewContainerRef} from "@angular/core";
import {ComponentFixture, TestBed} from "@angular/core/testing";

import {} from "jasmine";
import {of} from "rxjs";

import {RoleCheckDirective} from "./role-check.directive";
import {UserEntity} from "../user.entity";
import {UserService} from "../user.service";
import {RoleEntity} from "./role.entity";

/**
 * A suite of unit tests for the {@link RoleCheckDirective}.
 *
 * @since 1.0.0
 */
describe("RoleCheckDirective Tests", () => {
  @Component({
    selector: "role-check-test",
    template: `
      <div id="example-container">
        <a id="goo-link" *hciHasRole="'goo'">goo</a>
        <a id="foo-link" *hciHasRole="'foo'">foo</a>
      </div>
    `
  })
  class TestRoleCheckComponent {
  }

  let fixture: ComponentFixture<TestRoleCheckComponent>;
  let element: any;
  let authUser: UserEntity;

  class MockUserService {
    getAuthenticatedUser() {
      return of(authUser);
    }
  }

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        ViewContainerRef
      ],
      declarations: [RoleCheckDirective, TestRoleCheckComponent]
    });

    TestBed.overrideDirective(RoleCheckDirective, {
      set: {
        providers: [
          {
            provide: UserService,
            useValue: new MockUserService()
          }
        ]
      }
    });
  });

  it("Should create an embedded view from the decorated template with foo and goo links when user has both foo and goo roles.",
    () => {
      authUser = new UserEntity("42", "thugnificent", [
        new RoleEntity("foo"),
        new RoleEntity("goo")
      ]);
      fixture = TestBed.createComponent(TestRoleCheckComponent);
      fixture.detectChanges();
      element = fixture.nativeElement;

      expect(element.querySelectorAll("#example-container > a").length).toBe(2);
      expect(element.querySelector("#foo-link")).toBeDefined();
      expect(element.querySelector("#goo-link")).toBeDefined();
    });

  it("Should create an embedded view from the decorated template with a foo link when user only has the foo role.",
    () => {
      authUser = new UserEntity("42", "thugnificent", [
        new RoleEntity("foo"),
        new RoleEntity("bar")
      ]);
      fixture = TestBed.createComponent(TestRoleCheckComponent);
      fixture.detectChanges();
      element = fixture.nativeElement;

      expect(element.querySelectorAll("#example-container > a").length).toBe(1);
      expect(element.querySelector("#foo-link")).toBeDefined();
    });

  it("Should create an embedded view from the decorated template with a goo link when user only has the goo role.",
    () => {
      authUser = new UserEntity("42", "thugnificent", [
        new RoleEntity("goo"),
        new RoleEntity("baz")
      ]);
      fixture = TestBed.createComponent(TestRoleCheckComponent);
      fixture.detectChanges();
      element = fixture.nativeElement;

      expect(element.querySelectorAll("#example-container > a").length).toBe(1);
      expect(element.querySelector("#goo-link")).toBeDefined();
    });

  it("Should create an embedded view from the decorated template with no links when user does not have the foo or goo roles.",
    () => {
      authUser = new UserEntity("42", "thugnificent", [
        new RoleEntity("bar"),
        new RoleEntity("baz")
      ]);
      fixture = TestBed.createComponent(TestRoleCheckComponent);
      fixture.detectChanges();
      element = fixture.nativeElement;

      expect(element.querySelectorAll("#example-container > a").length).toBe(0);
    });

  it("Should create an embedded view from the decorated template with no links when user roles are null.",
    () => {
      authUser = new UserEntity("42", "thugnificent", null);
      fixture = TestBed.createComponent(TestRoleCheckComponent);
      fixture.detectChanges();
      element = fixture.nativeElement;

      expect(element.querySelectorAll("#example-container > a").length).toBe(0);
    });

  it("Should create an embedded view from the decorated template with no links when user is null.",
    () => {
      fixture = TestBed.createComponent(TestRoleCheckComponent);
      fixture.detectChanges();
      element = fixture.nativeElement;

      expect(element.querySelectorAll("#example-container > a").length).toBe(0);
    });
});
