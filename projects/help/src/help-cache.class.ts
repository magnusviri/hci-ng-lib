/*
 * Copyright (c) 2016 Huntsman Cancer Institute at the University of Utah, Confidential and Proprietary
 */
import {HelpEntity} from "./help.entity";

/**
 * A cache store with {@code Map} semantics for application cache management of {@link HelpEntity} objects. However,
 * the delegated data structure is an array storing a tuple of {@code [HelpEntity.Key, HelpEntity]}. This array is managed
 * in a way to behave as a circular or ring array. The result of this management is that once the maximum
 * size for the array is reached, the oldest cached tuple will be dropped when the next one is added. If the dropped detail
 * is required again, the client will have to obtain it from the server where, hopefully, browser caching will come into
 * play ({@see https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/ETag}). The default maximum size is '10'.
 * <p>
 * This application cache was included with the understanding that help detail will be largely stable and that the notion
 * of eventual consistency for the help details is accpetable in all the various application contexts. Using this strategy
 * eliminates any server communication to determine whether or not cached entities are valid (e.g. using ETAGs) improving
 * client application performance (which can be significant given the client specifications). This approach should be used
 * in conjunction with browser caching strategies as mentioned above. If these assumptions are invalid, then eliminating
 * this application cache might be the right course of action.
 * <p>
 * This cache will be cleared by a browser refresh, and should be cleared when a user session is terminated.
 *
 * @since 1.0.0
 */
export class HelpCache {
  /**
   * The maximum default cache size for this application cache.
   *
   * @type {number}
   */
  private static readonly DEFAULT_MAX_SIZE: number = 10;

  private _delegate: Array<[HelpEntity.Key, HelpEntity]> = [];

  constructor(private _maxSize?: number) {
    if(!this._maxSize) {
      this._maxSize = HelpCache.DEFAULT_MAX_SIZE;
    }
  }

  /**
   * Put the {@link HelpEntity} into this cache with the submitted {@link HelpEntity.Key}. Duplicate values will be stored
   * in the underlying data structure.
   * @param key
   * @param helpEntity
   */
  public put(key: HelpEntity.Key, helpEntity: HelpEntity): void {
    let existing: [HelpEntity.Key, HelpEntity] = this.get(key);

    if (existing) {
      existing[1] = helpEntity;
    } else if(this._delegate.length >= this._maxSize) {
      this._delegate.pop();
    }

    this._delegate.unshift([key, helpEntity]);
  }

  /**
   * Gets the first {@link HelpEntity} associated with the submitted {@link HelpEntity.Key}. If no entity exists with the
   * submitted key the response will be undefined.
   *
   * @param key for a cached {@code HelpEntity}
   * @returns {any} the cached {@code HelpEntity} if it exists or 'undefined'
   */
  public get(key: HelpEntity.Key): [HelpEntity.Key, HelpEntity] {
    return this._delegate.find((entry: [HelpEntity.Key, HelpEntity]) => {
      return key.equals(entry[0]);
    });
  }

  /**
   * Clears the cache.
   */
  public clear(): void {
    this._delegate = [];
  }
}
