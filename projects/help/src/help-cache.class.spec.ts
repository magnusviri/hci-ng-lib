/*
 * Copyright (c) 2016 Huntsman Cancer Institute at the University of Utah, Confidential and Proprietary
 */
import {} from "jasmine";

import {HelpCache} from "./help-cache.class";
import {HelpEntity} from "./help.entity";
import {ApplicationContextEnum} from "./application-context.enum";

/**
 * @since 1.0.0
 */
describe("HelpCache Tests", () => {
  const gnomexAppCtxt: string = ApplicationContextEnum[0];
  const oneKey: HelpEntity.Key = new HelpEntity.Key(1, gnomexAppCtxt);
  const oneHelpEntity: HelpEntity = new HelpEntity("foo", "foo title", "foo body");
  const twoKey: HelpEntity.Key = new HelpEntity.Key(2, gnomexAppCtxt);
  const twoHelpEntity: HelpEntity = new HelpEntity("goo", "goo title", "goo body");
  const threeKey: HelpEntity.Key = new HelpEntity.Key(3, gnomexAppCtxt);
  const threeHelpEntity: HelpEntity = new HelpEntity("bar", "bar title", "bar body");

  it("Should return 'undefined' when attempting to retrieve an entity that is not contained.", () => {
    let helpCache: HelpCache = new HelpCache();

    expect(helpCache.get(new HelpEntity.Key(42, gnomexAppCtxt))).toBeUndefined();
  });

  it("Should return cached HelpEntity if it is contained", () => {
    let helpCache: HelpCache = new HelpCache();

    helpCache.put(oneKey, oneHelpEntity);

    expect(helpCache.get(new HelpEntity.Key(1, gnomexAppCtxt))[1]).toBe(oneHelpEntity);
  });

  it("Should update a cached representation if exists with the submitted key.", () => {
    let helpCache: HelpCache = new HelpCache();

    helpCache.put(oneKey, oneHelpEntity);
    let he: [HelpEntity.Key, HelpEntity] = helpCache.get(oneKey);

    expect(he[1].Tooltip).toBe(oneHelpEntity.Tooltip);
    expect(he[1].Title).toBe(oneHelpEntity.Title);
    expect(he[1].Body).toBe(oneHelpEntity.Body);

    let newHe = new HelpEntity("new tooltip", "new title", "new body");

    helpCache.put(oneKey, newHe);
    let he2: [HelpEntity.Key, HelpEntity] = helpCache.get(oneKey);

    expect(he2[1].Tooltip).toBe(newHe.Tooltip);
    expect(he2[1].Title).toBe(newHe.Title);
    expect(he2[1].Body).toBe(newHe.Body);
  });

  it("Should only retain the number of HelpEntity objects as defined by the size limit, discarding the oldest when the " +
    "maximum is reached.", () => {
    let helpCache: HelpCache = new HelpCache(2);

    helpCache.put(oneKey, oneHelpEntity);
    helpCache.put(twoKey, twoHelpEntity);

    expect(helpCache.get(oneKey)[1]).toBe(oneHelpEntity);
    expect(helpCache.get(twoKey)[1]).toBe(twoHelpEntity);

    helpCache.put(threeKey, threeHelpEntity);

    expect(helpCache.get(oneKey)).toBeUndefined();
    expect(helpCache.get(twoKey)[1]).toBe(twoHelpEntity);
    expect(helpCache.get(threeKey)[1]).toBe(threeHelpEntity);
  });

  it("Should empty any cached items if HelpCache#clear is called.", () => {
    let helpCache: HelpCache = new HelpCache();

    helpCache.put(oneKey, oneHelpEntity);
    helpCache.put(twoKey, twoHelpEntity);
    helpCache.put(threeKey, threeHelpEntity);

    expect(helpCache.get(oneKey)[1]).toBe(oneHelpEntity);
    expect(helpCache.get(twoKey)[1]).toBe(twoHelpEntity);
    expect(helpCache.get(threeKey)[1]).toBe(threeHelpEntity);

    helpCache.clear();

    expect(helpCache.get(oneKey)).toBeUndefined();
    expect(helpCache.get(twoKey)).toBeUndefined();
    expect(helpCache.get(threeKey)).toBeUndefined();
  });

  it("Should have a default max size of '10'.", () => {
    let helpCache: HelpCache = new HelpCache();
    let zero: HelpEntity.Key = new HelpEntity.Key(0, gnomexAppCtxt);
    let two: HelpEntity.Key = new HelpEntity.Key(2, gnomexAppCtxt);
    let three: HelpEntity.Key = new HelpEntity.Key(3, gnomexAppCtxt);
    let four: HelpEntity.Key = new HelpEntity.Key(4, gnomexAppCtxt);
    let five: HelpEntity.Key = new HelpEntity.Key(5, gnomexAppCtxt);
    let six: HelpEntity.Key = new HelpEntity.Key(6, gnomexAppCtxt);
    let seven: HelpEntity.Key = new HelpEntity.Key(7, gnomexAppCtxt);
    let eight: HelpEntity.Key = new HelpEntity.Key(8, gnomexAppCtxt);
    let nine: HelpEntity.Key = new HelpEntity.Key(9, gnomexAppCtxt);
    let ten: HelpEntity.Key = new HelpEntity.Key(10, gnomexAppCtxt);

    helpCache.put(oneKey, oneHelpEntity);
    helpCache.put(zero, new HelpEntity());
    helpCache.put(two, new HelpEntity());
    helpCache.put(three, new HelpEntity());
    helpCache.put(four, new HelpEntity());
    helpCache.put(five, new HelpEntity());
    helpCache.put(six, new HelpEntity());
    helpCache.put(seven, new HelpEntity());
    helpCache.put(eight, new HelpEntity());
    helpCache.put(nine, new HelpEntity());

    expect(helpCache.get(oneKey)[1]).toBe(oneHelpEntity);

    helpCache.put(ten, new HelpEntity());

    expect(helpCache.get(oneKey)).toBeUndefined();
    expect(helpCache.get(zero)).toBeDefined();
    expect(helpCache.get(two)).toBeDefined();
    expect(helpCache.get(three)).toBeDefined();
    expect(helpCache.get(four)).toBeDefined();
    expect(helpCache.get(five)).toBeDefined();
    expect(helpCache.get(six)).toBeDefined();
    expect(helpCache.get(seven)).toBeDefined();
    expect(helpCache.get(eight)).toBeDefined();
    expect(helpCache.get(nine)).toBeDefined();
    expect(helpCache.get(ten)).toBeDefined();
  });
});
