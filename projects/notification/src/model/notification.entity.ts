export class Notification {
  idNotification: number;
  notification: string;
  type: string;
  status: string;
  html: boolean = false;
}
