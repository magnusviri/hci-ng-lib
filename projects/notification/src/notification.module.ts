/*
 * Copyright (c) 2016 Huntsman Cancer Institute at the University of Utah, Confidential and Proprietary
 */
import {ModuleWithProviders, NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";

import {NgbModule} from "@ng-bootstrap/ng-bootstrap";

import {NavigationModule} from "@huntsman-cancer-institute/navigation";

import {NotificationService, NOTIFICATION_CONFIG} from "./notification.service";
import {NotificationPopupComponent} from "./components/notification-popup.component";
import {NotificationItemComponent} from "./components/notification-item.component";
import {NotificationFilterPipe} from "./util/notification-filter.pipe";
import {NotificationDropdown} from "./components/notification-dropdown.component";

/**
 *
 */
@NgModule({
  imports: [
    CommonModule,
    NavigationModule,
    NgbModule
  ],
  declarations: [
    NotificationPopupComponent,
    NotificationItemComponent,
    NotificationDropdown,
    NotificationFilterPipe
  ],
  entryComponents: [
    NotificationDropdown
  ],
  exports: [
    NotificationPopupComponent,
    NotificationItemComponent
  ]
})
export class NotificationModule {
  static forRoot(notificationConfig: any): ModuleWithProviders<NotificationModule> {
    return {
      providers: [
        NotificationService,
        {provide: NOTIFICATION_CONFIG, useValue: notificationConfig}
      ],
      ngModule: NotificationModule
    };
  }
}
