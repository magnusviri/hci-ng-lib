import {
  ChangeDetectorRef, Component, ComponentFactoryResolver, ElementRef, Renderer2,
  OnInit, OnDestroy
} from "@angular/core";

import { Subject, Subscription } from "rxjs";

import {NavComponent, NavigationService} from "@huntsman-cancer-institute/navigation";

import {NotificationService} from "../notification.service";
import {Notification} from "../model/notification.entity";

@Component({
  selector: "hci-notification-dropdown",
  template: `
    <li [id]="id + '-li'" class="hci-li-dd-li {{liClass}}" ngbDropdown>
      <a id="notification-dropdown" class="hci-li-dd-a {{aClass}}" ngbDropdownToggle>
        <i class="{{iClass}}"></i>
        <hci-badge *ngIf="badgeCount" [countSubject]="badgeCount"></hci-badge>
      </a>
      <ul ngbDropdownMenu aria-labelledby="notification-dropdown" class="hci-li-dd-ul notification-dropdown-menu {{ulClass}}">
        <div class="d-flex">
          <div class="btn-group" role="group" aria-label="View Options" (click)="$event.stopPropagation()">
            <button type="button" class="btn btn-secondary">View {{viewMode}}</button>
            <div class="btn-group" role="group" ngbDropdown #viewDropdown="ngbDropdown">
              <button id="view" type="button" class="btn btn-secondary dropdown-toggle" ngbDropdownToggle>
                <i class="fas fa-arrow-circle-down"></i>
              </button>
              <div class="dropdown-menu dropdown-menu-right" aria-labelledby="view" ngbDropdownMenu>
                <a class="dropdown-item" (click)="viewDropdown.close(); viewMode = 'All'">All</a>
                <a class="dropdown-item" (click)="viewDropdown.close(); viewMode = 'Read'">Read</a>
                <a class="dropdown-item" (click)="viewDropdown.close(); viewMode = 'Unread'">Unread</a>
              </div>
            </div>
          </div>
          <div class="btn-group right" role="group" aria-label="Clear Options" (click)="$event.stopPropagation()">
            <button type="button" class="btn btn-secondary" (click)="clear()">Clear {{clearMode}}</button>
            <div class="btn-group" role="group" ngbDropdown #clearDropdown="ngbDropdown">
              <button id="clear"
                      type="button"
                      class="btn btn-secondary dropdown-toggle"
                      ngbDropdownToggle>
                <i class="fas fa-arrow-circle-down"></i>
              </button>
              <div class="dropdown-menu dropdown-menu-right" aria-labelledby="clear" ngbDropdownMenu>
                <a class="dropdown-item" (click)="clearDropdown.close(); clearMode = 'All'; clear();">All</a>
                <a class="dropdown-item" (click)="clearDropdown.close(); clearMode = 'Read'; clear();">Read</a>
                <a class="dropdown-item" (click)="clearDropdown.close(); clearMode = 'Unread'; clear();">Unread</a>
              </div>
            </div>
          </div>
        </div>
        <div class="notification-item-container">
          <ng-container *ngFor="let notification of notifications | notificationFilter:viewMode">
            <hci-notification-item [notification]="notification"></hci-notification-item>
          </ng-container>
          <ng-container *ngIf="(notifications | notificationFilter:viewMode).length === 0">
            <div class="empty">
              (No matching notifications)
            </div>
          </ng-container>
        </div>
      </ul>
    </li>
  `,
  host: {class: "hci-notification-dropdown"}
})
export class NotificationDropdown extends NavComponent implements OnInit, OnDestroy {

  public liClass: string = null;
  public aClass: string = "nav-link";
  public iClass: string = null;
  public ulClass: string = "dropdown-menu";

  viewMode: string = "Unread";
  clearMode: string = "Read";
  badgeCount: Subject<number> = new Subject<number>();
  notifications: Notification[] = [];

  notificationsSubscription: Subscription;

  constructor(elementRef: ElementRef,
              renderer: Renderer2,
              resolver: ComponentFactoryResolver,
              navigationService: NavigationService,
              changeDetectorRef: ChangeDetectorRef,
              private notificationService: NotificationService) {
    super(elementRef, renderer, resolver, navigationService, changeDetectorRef);
  }

  ngOnInit(): void {
    this.notificationsSubscription = this.notificationService.getNotificationsSubject().subscribe((notifications: Notification[]) => {
      this.notifications = notifications;
      this.badgeCount.next(notifications.filter((o: Notification) => {
        return o.status === "Unread";
      }).length);
    });
  }

  ngOnDestroy(): void {
    this.notificationsSubscription.unsubscribe();
  }

  clear() {
    if (this.clearMode === "All") {
      this.notificationService.clearAll();
    } else if (this.clearMode === "Read") {
      this.notificationService.clearRead();
    } else if (this.clearMode === "Unread") {
      this.notificationService.clearUnread();
    }
  }

  setConfig(config) {
    this.rootClass = "nav-item";

    super.setConfig(config);
  }

  updateConfig(config) {
    super.updateConfig(config);

    if (config.liClass) {
      this.liClass = config.liClass;
    }
    if (config.aClass) {
      this.aClass = config.aClass;
    }
    if (config.iClass) {
      this.iClass = config.iClass;
    }
    if (config.ulClass) {
      this.ulClass = config.ulClass;
    }
  }
}
