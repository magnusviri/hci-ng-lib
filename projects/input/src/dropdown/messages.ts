/**
 * A model of common messages for dropdown components.
 *
 * @since 5.0.0
 */
export class Messages {

  static readonly PARTIAL_COUNT_VAR = "%PARTIAL_COUNT%";
  static readonly TOTAL_COUNT_VAR = "%TOTAL_COUNT%";

  moreResultsMsg?: string;
  noResultsMsg?: string;
}
