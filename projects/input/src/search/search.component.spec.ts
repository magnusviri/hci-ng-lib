import {TestBed, async} from "@angular/core/testing";
import {CommonModule} from "@angular/common";
import {FormsModule} from "@angular/forms";

import {NgbDateStruct, NgbModule} from "@ng-bootstrap/ng-bootstrap";

import {SearchComponent} from "./search.component";
import {TreeModule, TreeComponent} from "@circlon/angular-tree-component";


describe("SearchComponent Tests", () => {

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        CommonModule,
        FormsModule,
        TreeModule,
        NgbModule
      ],
      declarations: [
        SearchComponent
      ]
    });
  });

  it ("search component - array filter test.",
    async(() => {

      let fixture = TestBed.createComponent(SearchComponent);
      let cmpt = fixture.componentInstance;

      cmpt.data = [
        { id: 1, name: "list item 1" },
        { id: 2, name: "list item 2" },
        { id: 3, name: "list item 3" },
        { id: 4, name: "list item 4" }
      ];

      cmpt.isArray = true;

      cmpt.dataFiltered.subscribe((list: any[]) => {
        expect (list).toEqual([
          { id: 2, name: "list item 2" }
        ]);
      });

      cmpt.doSearch("2");
    })
  );

  it ("search component - tree filter test.",
    async(() => {

      let tree = TestBed.createComponent(TreeComponent);
      let treecmpt = tree.componentInstance;

      let fixture = TestBed.createComponent(SearchComponent);
      let cmpt = fixture.componentInstance;

      cmpt.data = [
        {
          id: 1,
          name: "root tree 1",
          expanded: true,
          children: [
            { id: 2, name: "child a 2 e" },
            { id: 3, name: "child a 3 f" }
          ]
        },
        {
          id: 4,
          name: "root 4",
          expanded: true,
          children: [
            { id: 5, name: "child b 5 e" },
            { id: 6, name: "child b 6 f" }
          ]
        },
        {
          id: 7,
          name: "root 7",
          expanded: true,
          children: [
            { id: 8, name: "child c 8 e" },
            {
              id: 9,
              name: "root 9",
              expanded: true,
              children: [
                { id: 10, name: "child d 10 e" },
                { id: 11, name: "child d 11 f" }
              ]
            }
          ]
        },
        { id: 12, name: "root 12" }
      ];

      cmpt.input = "nodes";
      cmpt.anyComponent = treecmpt;

      cmpt.dataFiltered.subscribe((treeReturn: any[]) => {
        expect (treeReturn).toEqual([
          {
            id: 1,
            name: "root tree 1",
            expanded: true,
            children: [
              { id: 2, name: "child a 2 e" }
            ]
          },
          { id: 12, name: "root 12" }
        ]);
      });

      cmpt.doSearch("2");
    })
  );

  it ("search component - tree filter test - include children option",
    async(() => {

      let tree = TestBed.createComponent(TreeComponent);
      let treecmpt = tree.componentInstance;

      let fixture = TestBed.createComponent(SearchComponent);
      let cmpt = fixture.componentInstance;

      cmpt.data = [
        {
          id: 1,
          name: "root tree 1",
          expanded: true,
          children: [
            { id: 2, name: "child a 2 e" },
            { id: 3, name: "child a 3 f" }
          ]
        },
        {
          id: 4,
          name: "root 4",
          expanded: true,
          children: [
            { id: 5, name: "child b 5 e" },
            { id: 6, name: "child b 6 f" }
          ]
        },
        {
          id: 7,
          name: "root 7",
          expanded: true,
          children: [
            { id: 8, name: "child c 8 e" },
            {
              id: 9,
              name: "root 9",
              expanded: true,
              children: [
                { id: 10, name: "child d 10 e" },
                { id: 11, name: "child d 11 f" }
              ]
            }
          ]
        },
        { id: 12, name: "root 12" }
      ];

      cmpt.isIncludeChildren = true;
      cmpt.input = "nodes";
      cmpt.anyComponent = treecmpt;

      cmpt.dataFiltered.subscribe((treeReturn: any[]) => {
        expect (treeReturn).toEqual([
          {
            id: 1,
            name: "root tree 1",
            expanded: true,
            children: [
              { id: 2, name: "child a 2 e" },
              { id: 3, name: "child a 3 f" }
            ]
          },
          {
            id: 7,
            name: "root 7",
            expanded: true,
            children: [
              {
                id: 9,
                name: "root 9",
                expanded: true,
                children: [
                  { id: 10, name: "child d 10 e" },
                  { id: 11, name: "child d 11 f" }
                ]
              }
            ]
          },
          { id: 12, name: "root 12" }
        ]);
      });

      cmpt.doSearch("1");
    })
  );

});
