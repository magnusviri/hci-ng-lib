import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {FormsModule} from "@angular/forms";

import {InlineComponent} from "./inline.component";

@NgModule({
  imports: [
    CommonModule,
    FormsModule
  ],
  declarations: [
    InlineComponent
  ],
  exports: [
    InlineComponent
  ]
})
export class InlineModule {}
