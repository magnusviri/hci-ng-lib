import {
  Component, ContentChild, forwardRef, HostListener, Input, isDevMode, SimpleChange,
  TemplateRef
} from "@angular/core";
import {ControlValueAccessor, NG_VALUE_ACCESSOR, NgModel} from "@angular/forms";

/**
 * This component is designed to take any input as ng-content and wrap it such that it renders as a string.  When hovering
 * over, a border and edit icon is shown.  Upon clicking, it enters edit mode and the passed in input is rendered.  The
 * bound ngModel is wrapped in an intermediary.  This component binds a separate internal value to the wrapped component.
 * This allows the user to cancel out and not update the ngModel bound to this component.
 */
@Component({
  selector: "hci-inline-input",
  template: `
    <div *ngIf="!editMode; else editTemplate"
         (click)="setEditMode($event, true)"
         class="edit-false pl-1">
      <ng-container *ngIf="renderTemplate; else defaultRenderTemplate">
        <ng-container *ngTemplateOutlet="renderTemplate; context: {value: value}"></ng-container>
      </ng-container>
      <ng-template #defaultRenderTemplate>
        <ng-container *ngIf="value">
          {{value}}
        </ng-container>
        <ng-container *ngIf="!value">
          {{noValueMessage}}
        </ng-container>
      </ng-template>
      <button class="btn btn-primary ml-auto">
        <i class="fas fa-pencil-alt"></i>
      </button>
    </div>

    <ng-template #editTemplate>
      <div class="edit-true">
        <div class="input-container">
          <ng-content></ng-content>
        </div>
        <div class="d-flex">
          <button class="btn btn-green" (click)="save()">
            <i class="fas fa-check"></i>
          </button>
          <button class="btn btn-red d-flex" (click)="cancel()">
            <i class="fas fa-times ml-auto mr-auto"></i>
          </button>
        </div>
      </div>
    </ng-template>
  `,
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => InlineComponent),
      multi: true
    }
  ]
})
export class InlineComponent implements ControlValueAccessor {

  @ContentChild("renderTemplate", {read: TemplateRef, static: true}) renderTemplate: TemplateRef<any>;

  @Input() noValueMessage: string = "Click to Add";

  // View state
  editMode: boolean = false;

  // Value that updates based on this ngModel.
  value: any;

  // The value we bind to the passed in input component.
  modifiedValue: any;

  // Store the ngModel directive on the passed in input component.
  ngModelRef: NgModel;

  // Functions to register from Angular
  onChange: (value: any) => void;
  onTouched: () => void;

  /**
   * Called when the model updates and we need to up the view.
   *
   * @param value
   */
  writeValue(value: any) {
    if (isDevMode()) {
      console.debug("InlineComponent: writeValue");
      console.debug("InlineComponent: " + value);
    }

    // Update the internal values.
    this.value = value;
    this.modifiedValue = this.value;

    if (this.ngModelRef !== undefined) {
      if (isDevMode()) {
        console.debug("InlineComponent: update ngModelRef: " + this.modifiedValue);
      }

      // Update the model, then tell the directive that it has been updated.
      this.ngModelRef.model = this.modifiedValue;
      this.ngModelRef.ngOnChanges({
        model: new SimpleChange(undefined, this.modifiedValue, false)
      });
    }
  }

  /**
   * Register the function that is called when the value changes in the value.
   *
   * @param {(value: any) => void} fn
   */
  registerOnChange(fn: (value: any) => void) {
    if (isDevMode()) {
      console.debug("InlineComponent: registerOnChange");
      console.debug(fn);
    }

    this.onChange = () => {
      if (isDevMode()) {
        console.debug("InlineComponent: onChange Function" + this.value);
      }
      fn(this.value);
    };
  }

  /**
   * Register the function called to update the model on blur.
   *
   * @param {() => void} fn
   */
  registerOnTouched(fn: () => void): void {
    if (isDevMode()) {
      console.debug("InlineComponent: registerOnTouched");
      console.debug(fn);
    }
    this.onTouched = fn;
  }

  /**
   * Set the editMode.  If true then tell the passed in input to bind the current modified value.
   *
   * @param {boolean} editMode
   */
  setEditMode(event: MouseEvent, editMode: boolean): void {
    event.stopPropagation();

    this.editMode = editMode;
    if (this.editMode) {
      this.onTouched();

      this.ngModelRef.model = this.modifiedValue;
      this.ngModelRef.ngOnChanges({
        model: new SimpleChange(undefined, this.modifiedValue, false)
      });
    }
  }

  /**
   * Cancel edit mode which resets the modified value.
   */
  cancel(): void {
    this.editMode = false;
    this.modifiedValue = this.value;
  }

  /**
   * Update the value with the modified value and call the onChange function so two way binding is updated with this component.
   */
  save(): void {
    if (isDevMode()) {
      console.debug("InlineComponent: save modifiedValue: " + this.modifiedValue + ", value: " + this.value);
    }

    this.value = this.modifiedValue;
    this.onChange(this.value);
    this.editMode = false;
  }

  /**
   * The component to wrap must be named with #input and have a ngModel directive.  When the content is set here, we
   * read the ngModel and set the model as well as listen to changes.
   *
   * @param {NgModel} ngModel
   */
  @ContentChild("input", {read: NgModel, static: false}) set contentChild(ngModel: NgModel) {
    if (isDevMode()) {
      console.debug("InlineComponent: set contentChild: " + this.modifiedValue);
      console.debug(ngModel);
    }

    this.ngModelRef = ngModel;
    if (this.ngModelRef) {
      this.ngModelRef.model = this.modifiedValue;
      this.ngModelRef.ngOnChanges({
        model: new SimpleChange(undefined, this.modifiedValue, false)
      });
      this.ngModelRef.update.subscribe((value: any) => {
        if (isDevMode()) {
          console.debug("InlineComponent: cmpt model change: " + value);
        }
        this.modifiedValue = value;
      })
    }
  }

  /**
   * If the click target is outside the inline input then cancel it.
   *
   * @param {MouseEvent} event
   */
  @HostListener("document:click", ["$event"])
  documentClick(event: MouseEvent): void {
    if (this.editMode) {
      if (isDevMode()) {
        console.debug("InlineComponent: documentClick");
      }

      let e: Element = (<HTMLElement>event.target).closest("hci-inline-input");
      if (!e) {
        this.cancel();
      }
    }
  }
}
