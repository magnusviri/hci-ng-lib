import {TestBed, async} from "@angular/core/testing";
import {CommonModule} from "@angular/common";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";

import {NgbModule} from "@ng-bootstrap/ng-bootstrap";

import {DateComponent} from "./date-date.component";

describe("InlineDateComponent Tests", () => {

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        NgbModule
      ],
      declarations: [
        DateComponent
      ]
    });
  });

  it ("Should show datepicker in state 2.",
    async(() => {
      let fixture = TestBed.createComponent(DateComponent);
      let cmpt = fixture.componentInstance;

      cmpt.startEdit();

      fixture.detectChanges();
      let element = fixture.nativeElement;

      expect(element.querySelectorAll("div > ngb-datepicker").length).toBe(1);
    })
  );

  it("Should update inputData upon save().",
    async(() => {
      let fixture = TestBed.createComponent(DateComponent);
      let cmpt = fixture.componentInstance;

      cmpt.inputData = "2018-01-18T12:00-06:00";
      cmpt.ngOnInit();
      cmpt.modifiedData = {year: 2018, month: 1, day: 18};
      cmpt.save();

      expect(cmpt.inputData).toEqual("2018-01-18T12:00-06:00");
    })
  );

  it("Should not update inputData upon cancel().",
    async(() => {
      let fixture = TestBed.createComponent(DateComponent);
      let cmpt = fixture.componentInstance;

      cmpt.inputData = "2018-01-18T12:00-06:00";
      cmpt.ngOnInit();
      cmpt.modifiedData = new Date("2017-01-18T12:00-06:00");
      cmpt.cancel();

      expect(cmpt.inputData).toEqual("2018-01-18T12:00-06:00");
    })
  );

});
