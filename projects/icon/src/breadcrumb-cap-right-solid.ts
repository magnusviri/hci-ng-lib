export const hciBreadcrumbCapRightSolid = {
  prefix: "hci",
  iconName: "breadcrumb-cap-right-solid",
  icon: [512, 512, [], "f128", "M0 0v512l186.3-256z"]
};
