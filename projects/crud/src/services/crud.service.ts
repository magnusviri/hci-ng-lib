import {Inject, Injectable, InjectionToken} from "@angular/core";
import {HttpClient, HttpParams, HttpResponse} from "@angular/common/http";

import {BehaviorSubject, Observable, of} from "rxjs";
import {finalize} from "rxjs/operators";

import {HciDataDto, HciGridDto, HciPostWrapperDto} from "@huntsman-cancer-institute/hci-ng-grid-dto";

export let CRUD_ENDPOINT = new InjectionToken<string>("crudEndpoint");

/**
 * This service manages communication with the crud api.
 * The typical api endpoint would be "/app-name/api/crud".
 *
 * TODO: A call that would return the response as well as the data.
 */
@Injectable()
export class CrudService {

  metadataMap: Map<string, Object> = new Map<string, Object>();

  constructor(private http: HttpClient, @Inject(CRUD_ENDPOINT) private crudEndpoint: string) {}

  public getMetadata(className: string): Observable<Object> {
    if (this.metadataMap.get(className)) {
      return of(this.metadataMap.get(className));
    }

    let o: Observable<Object> = this.http.get<Object>(this.crudEndpoint + className + "/metadata");
    o.subscribe((metadata: any) => {
      this.metadataMap.set(className, metadata);
    });
    return o;
  }

  /**
   * Makes the http request and returns an observable for it.
   *
   * @param {string} className
   * @param {HciPostWrapperDto} dto
   * @param {string} query
   * @returns {Observable<HciDataDto>}
   */
  public getAll(className: string, dto: HciPostWrapperDto = undefined, query: string = undefined): Observable<HciDataDto> {
    let params: HttpParams = new HttpParams();
    if (query) {
      params = params.set("query", query);
    }

    if (dto) {
      return this.http.post<HciDataDto>(this.crudEndpoint + className + "/info", dto, {params: params});
    } else {
      return this.http.get<HciDataDto>(this.crudEndpoint + className, {params: params});
    }
  }

  /**
   * Return a loading subject and require a callback to deal with setting data when the response comes back.
   *
   * @param {string} className The name of the entity or DTO.
   * @param {HciPostWrapperDto} dto Default to undefined.
   * @param {string} query Default to undefined.
   * @param {(data: HciDataDto) => any} onResponse Only required function.
   * @param {(error) => any} onError Optional
   * @param {() => any} onFinalize Optional
   * @returns {BehaviorSubject<boolean>}
   */
  public getAllLoading(className: string,
                       dto: HciPostWrapperDto = undefined,
                       query: string = undefined,
                       onResponse: (data: HciDataDto) => any,
                       onError?: (error) => any,
                       onFinalize?: () => any): BehaviorSubject<boolean> {
    let loading: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(true);

    let params: HttpParams = new HttpParams();
    if (query) {
      params = params.set("query", query);
    }

    if (dto) {
      this.http.post<HciDataDto>(this.crudEndpoint + className + "/info", dto, {params: params})
        .pipe(finalize(() => {
          if (onFinalize) {
            onFinalize();
          }
        }))
        .subscribe((data: HciDataDto) => {
          loading.next(false);
          onResponse(data);
        },
        (error) => {
          if (onError) {
            onError(error);
          }
        });
    } else {
      this.http.get<HciDataDto>(this.crudEndpoint + className, {params: params})
        .pipe(finalize(() => {
          if (onFinalize) {
            onFinalize();
          }
        }))
        .subscribe((data: HciDataDto) => {
            loading.next(false);
            onResponse(data);
          },
          (error) => {
            if (onError) {
              onError(error);
            }
          });
    }

    return loading;
  }

  /**
   * Get a single instance of an entity based on the id.
   *
   * @param {string} className
   * @param {number} id
   * @returns {Observable<Object>}
   */
  public get(className: string, id: number): Observable<Object> {
    return this.http.get<Object>(this.crudEndpoint + className + "/" + id);
  }

  /**
   * Return the loading subject for the request.  Callback required to get the entity response.
   *
   * @param {string} className
   * @param {number} id
   * @param {(data: Object) => any} onResponse
   * @param {(error) => any} onError
   * @param {() => any} onFinalize
   * @returns {BehaviorSubject<boolean>}
   */
  public getLoading(className: string,
                    id: number,
                    onResponse: (data: Object) => any,
                    onError?: (error) => any,
                    onFinalize?: () => any): BehaviorSubject<boolean> {
    let loading: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(true);

    this.http.get<Object>(this.crudEndpoint + className + "/" + id).subscribe((data: Object) => {
      loading.next(false);
      onResponse(data);
    },
    (error) => {
      onError(error);
    },
    () => {
      onFinalize();
    });

    return loading;
  }

  /**
   * Post a new entity.  One without an id.  Return that entity.
   *
   * @param {string} className
   * @param data
   * @returns {Observable<Object>}
   */
  public post(className: string, data: any): Observable<Object> {
    return this.http.post<Object>(this.crudEndpoint + className, data);
  }

  /**
   * Return a loading subject.  Post a new entity without an id.  Use the callback to get the entity response.
   *
   * @param {string} className
   * @param data
   * @param {(data: Object) => any} onResponse
   * @param {(error) => any} onError
   * @param {() => any} onFinalize
   * @returns {BehaviorSubject<boolean>}
   */
  public postLoading(className: string,
                     data: any,
                     onResponse: (data: Object) => any,
                     onError?: (error) => any,
                     onFinalize?: () => any): BehaviorSubject<boolean> {
    let loading: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(true);

    this.http.post<Object>(this.crudEndpoint + className, data).subscribe((data: Object) => {
      loading.next(false);
      onResponse(data);
    },
    (error) => {
      onError(error);
    },
    () => {
      onFinalize();
    });

    return loading;
  }

  /**
   * Put an existing entity with an id.  Return the updated entity.
   *
   * @param {string} className
   * @param {number} id
   * @param data
   * @returns {Observable<Object>}
   */
  public put(className: string, id: number, data: any): Observable<Object> {
    return this.http.put<Object>(this.crudEndpoint + className + "/" + id, data);
  }

  /**
   * Return the loading subject.  Put an existing entity with an id.  In the callback return the updated entity.
   *
   * @param {string} className
   * @param {number} id
   * @param data
   * @param {(data: Object) => any} onResponse
   * @param {(error) => any} onError
   * @param {() => any} onFinalize
   * @returns {BehaviorSubject<boolean>}
   */
  public putLoading(className: string,
                    id: number, data: any,
                    onResponse: (data: Object) => any,
                    onError?: (error) => any,
                    onFinalize?: () => any): BehaviorSubject<boolean> {
    let loading: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(true);

    this.http.put<Object>(this.crudEndpoint + className + "/" + id, data).subscribe((data: Object) => {
      loading.next(false);
      onResponse(data);
    },
    (error) => {
      onError(error);
    },
    () => {
      onFinalize();
    });

    return loading;
  }

  /**
   * Delete the entity by its id, return the response with no body.
   *
   * @param {string} className
   * @param {number} id
   * @returns {Observable<Object>}
   */
  public delete(className: string, id: number): Observable<HttpResponse<any>> {
    return this.http.delete<Object>(this.crudEndpoint + className + "/" + id, { observe: "response" });
  }

  /**
   * Return the loading subject.  Delete the entity by id, use the callback to get the response with no body.
   *
   * @param {string} className
   * @param {number} id
   * @param {(data: Object) => any} onResponse
   * @param {(error) => any} onError
   * @param {() => any} onFinalize
   * @returns {BehaviorSubject<boolean>}
   */
  public deleteLoading(className: string,
                       id: number,
                       onResponse: (response: HttpResponse<any>) => any,
                       onError?: (error) => any,
                       onFinalize?: () => any): BehaviorSubject<boolean> {
    let loading: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(true);

    this.http.delete<Object>(this.crudEndpoint + className + "/" + id, { observe: "response" }).subscribe((response: HttpResponse<any>) => {
      loading.next(false);
      onResponse(response);
    },
    (error) => {
      onError(error);
    },
    () => {
      onFinalize();
    });

    return loading;
  }
}
