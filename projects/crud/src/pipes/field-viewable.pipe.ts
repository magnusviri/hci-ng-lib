import {Pipe} from "@angular/core";

@Pipe({
  name: "fieldViewable",
  pure: true
})
export class FieldViewablePipe {

  transform(list: any[]): any {
    return list.filter((item: any) => {
      return item.viewable;
    });
  }
}
